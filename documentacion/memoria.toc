\select@language {spanish}
\contentsline {part}{I\hspace {1em}Proleg\IeC {\'o}meno}{13}{part.1}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{15}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivo del proyecto}{15}{section.1.1}
\contentsline {section}{\numberline {1.2}Glosario de T\IeC {\'e}rminos}{15}{section.1.2}
\contentsline {chapter}{\numberline {2}Planificaci\IeC {\'o}n}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}Metodolog\IeC {\'\i }a de desarrollo}{17}{section.2.1}
\contentsline {section}{\numberline {2.2}Planificaci\IeC {\'o}n del proyecto}{17}{section.2.2}
\contentsline {section}{\numberline {2.3}Diagrama de Gantt}{18}{section.2.3}
\contentsline {part}{II\hspace {1em}Desarrollo}{21}{part.2}
\contentsline {chapter}{\numberline {3}An\IeC {\'a}lisis y dise\IeC {\~n}o del Sistema}{23}{chapter.3}
\contentsline {section}{\numberline {3.1}Actores}{23}{section.3.1}
\contentsline {section}{\numberline {3.2}Clases abstractas}{23}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Pantalla}{23}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Juego}{23}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Modelo Conceptual}{24}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Men\IeC {\'u} idiomas}{24}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Men\IeC {\'u} principal}{25}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Men\IeC {\'u} opciones}{26}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Men\IeC {\'u} elecci\IeC {\'o}n de personajes y escenario}{27}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Lucha}{28}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}Men\IeC {\'u} Pausa}{29}{subsection.3.3.6}
\contentsline {subsection}{\numberline {3.3.7}Men\IeC {\'u} KO}{30}{subsection.3.3.7}
\contentsline {section}{\numberline {3.4}Modelo de Casos de Uso}{31}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Men\IeC {\'u} idiomas}{31}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Men\IeC {\'u} principal}{32}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Men\IeC {\'u} opciones}{33}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Men\IeC {\'u} elecci\IeC {\'o}n de personajes y escenario}{34}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Lucha}{36}{subsection.3.4.5}
\contentsline {subsection}{\numberline {3.4.6}Men\IeC {\'u} Pausa}{37}{subsection.3.4.6}
\contentsline {subsection}{\numberline {3.4.7}Men\IeC {\'u} KO}{38}{subsection.3.4.7}
\contentsline {section}{\numberline {3.5}Diagrama de secuencia}{39}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Men\IeC {\'u} idiomas}{39}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Men\IeC {\'u} principal}{40}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Men\IeC {\'u} opciones}{41}{subsection.3.5.3}
\contentsline {subsection}{\numberline {3.5.4}Men\IeC {\'u} elecci\IeC {\'o}n de personajes y escenario}{42}{subsection.3.5.4}
\contentsline {subsection}{\numberline {3.5.5}Lucha}{43}{subsection.3.5.5}
\contentsline {subsection}{\numberline {3.5.6}Men\IeC {\'u} Pausa}{44}{subsection.3.5.6}
\contentsline {subsection}{\numberline {3.5.7}Men\IeC {\'u} KO}{45}{subsection.3.5.7}
\contentsline {section}{\numberline {3.6}Contrato operaciones}{46}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Men\IeC {\'u} idiomas}{46}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}Men\IeC {\'u} principal}{46}{subsection.3.6.2}
\contentsline {subsection}{\numberline {3.6.3}Men\IeC {\'u} opciones}{46}{subsection.3.6.3}
\contentsline {subsection}{\numberline {3.6.4}Men\IeC {\'u} elecci\IeC {\'o}n de personajes y escenario}{46}{subsection.3.6.4}
\contentsline {subsection}{\numberline {3.6.5}Lucha}{47}{subsection.3.6.5}
\contentsline {subsection}{\numberline {3.6.6}Men\IeC {\'u} Pausa}{47}{subsection.3.6.6}
\contentsline {subsection}{\numberline {3.6.7}Men\IeC {\'u} KO}{47}{subsection.3.6.7}
\contentsline {section}{\numberline {3.7}Boceto pantalla}{48}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Men\IeC {\'u} idiomas}{48}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}Men\IeC {\'u} principal}{48}{subsection.3.7.2}
\contentsline {subsection}{\numberline {3.7.3}Men\IeC {\'u} opciones}{49}{subsection.3.7.3}
\contentsline {subsection}{\numberline {3.7.4}Men\IeC {\'u} elecci\IeC {\'o}n de personajes y escenario}{49}{subsection.3.7.4}
\contentsline {subsection}{\numberline {3.7.5}Lucha}{50}{subsection.3.7.5}
\contentsline {subsection}{\numberline {3.7.6}Men\IeC {\'u} Pausa}{50}{subsection.3.7.6}
\contentsline {subsubsection}{Men\IeC {\'u} KO}{51}{section*.8}
\contentsline {section}{\numberline {3.8}Resultado final}{52}{section.3.8}
\contentsline {subsection}{\numberline {3.8.1}Men\IeC {\'u} idiomas}{52}{subsection.3.8.1}
\contentsline {subsection}{\numberline {3.8.2}Men\IeC {\'u} principal}{53}{subsection.3.8.2}
\contentsline {subsection}{\numberline {3.8.3}Men\IeC {\'u} opciones}{53}{subsection.3.8.3}
\contentsline {subsection}{\numberline {3.8.4}Men\IeC {\'u} elecci\IeC {\'o}n de personajes y escenario}{54}{subsection.3.8.4}
\contentsline {subsection}{\numberline {3.8.5}Lucha}{54}{subsection.3.8.5}
\contentsline {subsection}{\numberline {3.8.6}Men\IeC {\'u} Pausa}{55}{subsection.3.8.6}
\contentsline {subsubsection}{Men\IeC {\'u} KO}{55}{section*.9}
\contentsline {section}{\numberline {3.9}Navegaci\IeC {\'o}n entre pantallas}{56}{section.3.9}
\contentsline {chapter}{\numberline {4}Construcci\IeC {\'o}n del Sistema}{57}{chapter.4}
\contentsline {section}{\numberline {4.1}Organizaci\IeC {\'o}n del repositorio}{57}{section.4.1}
\contentsline {section}{\numberline {4.2}Entorno de Construcci\IeC {\'o}n}{58}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Documentaci\IeC {\'o}n}{58}{subsection.4.2.1}
\contentsline {subsubsection}{Texmaker}{58}{section*.10}
\contentsline {subsubsection}{Dia}{58}{section*.11}
\contentsline {subsubsection}{Pencil evolus}{58}{section*.12}
\contentsline {subsubsection}{Gimp}{59}{section*.13}
\contentsline {subsubsection}{Slippy - HTML Slides (presentaci\IeC {\'o}n)}{59}{section*.14}
\contentsline {subsection}{\numberline {4.2.2}Videojuego}{59}{subsection.4.2.2}
\contentsline {subsubsection}{Blender}{59}{section*.15}
\contentsline {subsubsection}{Bgui}{59}{section*.16}
\contentsline {subsubsection}{Git}{59}{section*.17}
\contentsline {subsubsection}{Poedit}{59}{section*.18}
\contentsline {subsubsection}{Sublime Text 2}{60}{section*.19}
\contentsline {section}{\numberline {4.3}Bucle del juego}{60}{section.4.3}
\contentsline {section}{\numberline {4.4}Composici\IeC {\'o}n de los personajes}{60}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Malla (Mesh)}{60}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Preparaci\IeC {\'o}n de la malla}{62}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Esqueleto (Armature)}{63}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}Fisicas y colisiones}{65}{subsection.4.4.4}
\contentsline {subsection}{\numberline {4.4.5}Nomenclatura}{67}{subsection.4.4.5}
\contentsline {section}{\numberline {4.5}Mec\IeC {\'a}nicas de personajes}{67}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Movimientos}{67}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Acciones}{67}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Grafo}{68}{subsection.4.5.3}
\contentsline {subsection}{\numberline {4.5.4}IA}{69}{subsection.4.5.4}
\contentsline {section}{\numberline {4.6}Ficheros .json}{69}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Ejemplo json de un personaje}{69}{subsection.4.6.1}
\contentsline {section}{\numberline {4.7}Escenarios}{74}{section.4.7}
\contentsline {section}{\numberline {4.8}Elementos del Juego}{76}{section.4.8}
\contentsline {chapter}{\numberline {5}Pruebas del Sistema}{77}{chapter.5}
\contentsline {section}{\numberline {5.1}Funciones empleadas para las pruebas}{77}{section.5.1}
\contentsline {section}{\numberline {5.2}Ejemplos de uso}{78}{section.5.2}
\contentsline {section}{\numberline {5.3}log.txt}{79}{section.5.3}
\contentsline {section}{\numberline {5.4}Ejemplo}{79}{section.5.4}
\contentsline {part}{III\hspace {1em}Ep\IeC {\'\i }logo}{83}{part.3}
\contentsline {chapter}{\numberline {6}Manual de usuario}{85}{chapter.6}
\contentsline {section}{\numberline {6.1}Requisitos}{85}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Sitemas operativos}{85}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Requisitos m\IeC {\'\i }nimos de hardware}{85}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Requisitos recomendados de hardware}{85}{subsection.6.1.3}
\contentsline {section}{\numberline {6.2}Instalaci\IeC {\'o}n}{85}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Windows}{85}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Linux}{86}{subsection.6.2.2}
\contentsline {section}{\numberline {6.3}Controles}{86}{section.6.3}
\contentsline {section}{\numberline {6.4}Introducci\IeC {\'o}n}{86}{section.6.4}
\contentsline {section}{\numberline {6.5}Caracter\IeC {\'\i }sticas}{86}{section.6.5}
\contentsline {section}{\numberline {6.6}Requisitos previos}{87}{section.6.6}
\contentsline {section}{\numberline {6.7}Uso del sistema}{87}{section.6.7}
\contentsline {chapter}{\numberline {7}Conclusiones}{89}{chapter.7}
\contentsline {section}{\numberline {7.1}Objetivos alcanzados y lecciones aprendidas}{89}{section.7.1}
\contentsline {section}{\numberline {7.2}Futuras mejoras}{89}{section.7.2}
\contentsline {chapter}{Bibliograf\'{\i }a}{91}{chapter*.20}
\contentsline {chapter}{Informaci\IeC {\'o}n sobre Licencia}{93}{section*.21}
\contentsline {chapter}{GNU Free Documentation License}{93}{section*.22}
\contentsline {section}{1. APPLICABILITY AND DEFINITIONS}{93}{section*.23}
\contentsline {section}{2. VERBATIM COPYING}{94}{section*.24}
\contentsline {section}{3. COPYING IN QUANTITY}{95}{section*.25}
\contentsline {section}{4. MODIFICATIONS}{95}{section*.26}
\contentsline {section}{5. COMBINING DOCUMENTS}{97}{section*.27}
\contentsline {section}{6. COLLECTIONS OF DOCUMENTS}{97}{section*.28}
\contentsline {section}{7. AGGREGATION WITH INDEPENDENT WORKS}{97}{section*.29}
\contentsline {section}{8. TRANSLATION}{98}{section*.30}
\contentsline {section}{9. TERMINATION}{98}{section*.31}
\contentsline {section}{10. FUTURE REVISIONS OF THIS LICENSE}{98}{section*.32}
\contentsline {section}{11. RELICENSING}{98}{section*.33}
\contentsline {section}{ADDENDUM: How to use this License for your documents}{99}{section*.34}
\contentsfinish 
