# -*- coding: utf-8 -*-

from personaje import *
from eventosManager import *
from luchadorEstado import *
from audio import *

class Luchador(Personaje):
	def __init__(self, nombre, escenario):
		Personaje.__init__(self, nombre)
		self.escenario = escenario
		self.eventos = EventManager()
		self.estado = LuchadorSpawn(self)
		self.vida = 100
		self.chocaEnemigo_ = False
		self.distanciaChoque = None
		self.distanciaAlcance = None

		self.sonidoAndar = Sonido("fx/"+"138476__randomationpictures__step-tap.wav")
		#self.sonidoGolpeado = 
		self.sonidoGolpeAire = Sonido("fx/"+"melee sound.wav")
		self.sonidoGolpear = Sonido("fx/"+"89769__cgeffex__fist-punch-3.mp3")
		self.sonidoBloqueo = Sonido("fx/"+"131142_flameeagle_block.mp3")
		self.sonidoHerido = Sonido("fx/"+"163441__under7dude__man-getting-hit.wav")
		self.sonidoSalto = self.sonidoGolpeAire 

		
		self.sonidoKo =Sonido("fx/"+"276253__dynajinn__fight-voiceover-ko.wav")
		

	def recibirImpacto(self, impactable):#comprobar animacion reproduciendose
		self.elementosRagdoll[impactable].collisionCallbacks.append(self.luchadorGolpeado)

	def luchadorGolpeado(self, other):
		if (other.name != "Plane"):
			print("golpeado: ", other)
		if other in self.enemigo().estado.huesosGolpeador:
			self.estado.golpeado()

	def muroGolpeado(self, other):
		if (other.name != "Plane"):
			print("golpeado: ", other)
		if other ==self.muroEspaldas:
			return True
		else:
			return False

	def addMovimiento(self, event, status, callback):
		self.eventos.addListener(event, status, callback)


	def salto(self, salto = None):
		if (salto == None):
			return salto
		else :
			self.salto = salto*self.escala[0]

	def calcularMuro(self):
		if(self.direccionAvanzar()):
			self.muroEspaldas = self.escenario.muroIzq
		else:
			self.muroEspaldas = self.escenario.muroDer
	
	def direccionAvanzar(self):
		if (self.objeto.worldPosition[0]<self.enemigo().objeto.worldPosition[0]):
			return 1
		else:
			return -1
		
	####### OBSOLETO ########
	def capazAvanzar(self):

		if(self.distanciaChoque!= None):
			return self.objeto.getDistanceTo(self.enemigo().objeto) > self.distanciaChoque + self.distanciaChoque*0.01
			

		return True
		
	def perderVida(self, vida=100):
		self.vida -= vida
		print("vida: ",self.vida)

	def direccionRetroceder(self):
		return (-1 * self.direccionAvanzar())


	def capazRetroceder(self):
		return self.objeto.collisionCallbacks.append(self.muroGolpeado)

	def reorientacion(self):
		pass

	def debug(self): #Comprobar si solo son estos objetos los que puedo mirar en el debug
		self.objeto["x"] = self.objeto.worldPosition.x
		self.objeto["y"] = self.objeto.worldPosition.y
		self.objeto["z"] = self.objeto.worldPosition.z
		self.objeto["vz"] = self.objeto.getLinearVelocity()[2]

	def spawn(self, lugar):
		super().spawn(lugar)
		#Abrimos el json de nuestro personaje
		#ruta= bge.logic.expandPath("//") + "modelos/personajes/"
		#archivo = self.nombre+"_atributos.json"
		
		
		for i in self.huesosImpacto:
			print("impactable: ", self.elementosRagdoll[i].name)
			self.recibirImpacto(i)


	def update(self):
		super().update()
		#print("update lcuhador")
		""""""
		#print("aaa",self.huesosGolpeadores)
		for i in self.enemigo().estado.huesosGolpeador:
			#print("i", i)
			for j in self.huesosImpacto:
				#print("j", j)
				#print("distanciaa: " + self.elementosRagdoll[j].getDistanceTo(self.enemigo().elementosRagdoll[i]))
				#print(self.elementosRagdoll[i].getVectTo(self.enemigo().elementosRagdoll[j]))
				#if (self.elementosRagdoll[i].getVectTo(self.enemigo().elementosRagdoll[j])[2][0]<0):
					#print("x= ",self.elementosRagdoll[i].getVectTo(self.enemigo().elementosRagdoll[j])[0])
					
				#Da el pego pero hay que cambiar self.huesosGolpeadores segun el estado en que nos encontremos. (por ejemplo al protegerse se activa)
				
				if (self.elementosRagdoll[j].getDistanceTo(self.enemigo().elementosRagdoll[i])<1.8*self.escala[0]):
					print("EXITOOOOOO")
					self.estado.golpeado()
		
		#self.estadoCarga.update(self)
		self.eventos.update() #Este van en LuchadorJugador porque sino puedo controlar el bot
		self.estado.update() #Esto 
		#print("Nombre", self.nombre, "Dir avanzar:", self.direccionAvanzar(), "Dir retroceder", self.direccionRetroceder())
		#self.debug() # no funciona bien # el problema es otro
		#print(self.objeto.getLinearVelocity()[2])
		

	#Métodos observadores, cuidado: enemigos no solo es observador
	def enemigo(self, enemigo=None):#simulación de sobrecarga de función
		if (enemigo!=None):
			self.enemigo_ = enemigo
			#Controlamos si colisiona con enemigo
			#self.objeto.children["puntoColision"].visible =True
			self.objeto.children["puntoColision"].collisionCallbacks.append(self.alcanzaEnemigo)
			self.objeto.collisionCallbacks.append(self.chocaEnemigo)
			self.calcularMuro()
		else:
			return self.enemigo_

	def distanciaEnemigo(self):
		return self.objeto.getDistanceTo(self.enemigo().objeto)
		#return self.enemigo.distancia()
		#return distancia(self.personaje, self.enemigo)

	def vida(self):
		return self.vida
		
	def vidaEnemigo(self):
		return self.enemigo().vida()

	# True si choca con el enemigo (no podria seguir avanzando, False si no choca)
	def alcanzaEnemigo(self, other):
		if (other==self.enemigo().objeto):
			if(self.distanciaAlcance == None):
				self.distanciaAlcance = self.objeto.getDistanceTo(self.enemigo().objeto) # CAMBIAR x distancia de objeto, no del children
				self.objeto.children["puntoColision"].endObject()

		#return self.chocaEnemigo_
	def chocaEnemigo(self, other):
		if (other==self.enemigo().objeto):
			if(self.distanciaChoque == None):
				self.distanciaChoque = self.objeto.getDistanceTo(self.enemigo().objeto) # CAMBIAR x distancia de objeto, no del children
				
		

"""
Fuera de la clase para poder llamarlo como callback, 
consultar como poder hacer callback dentro de la clase
lo mismo a la hora de llamarlo solo hay que poner self.avanzar en lugar de avanzar simplemente

Ejemplo:
La primera respuesta de
http://stackoverflow.com/questions/897739/how-do-i-refer-to-a-class-method-outside-a-function-body-in-python
"""

def avanzar(personaje):
	personaje.estado.avanzar() #self.velocidad
	
def retroceder(personaje):
	personaje.estado.retroceder() #self.velocidad

def saltar(personaje):
	personaje.estado.saltar()

def golpe1(personaje):
	personaje.estado.golpe1()

def proteger(personaje):
	personaje.estado.proteger()

def retroceder(personaje):
	personaje.estado.retroceder()

def parar(personaje):
	personaje.estado.parar()

def reproducirAccion(personaje,accion):
	personaje.reproducirAccion(accion)

def reproducirAccionInversa(personaje,accion):
	personaje.reproducirAccionInversa(accion)
	
def activaRagdoll(personaje):
	personaje.activaRagdoll()
	
def desactivaRagdoll(personaje):
	personaje.desactivaRagdoll()

