# -*- coding: utf-8 -*-

import sys
import bge
import os
import math

#import struct #para usar dir y comprobar los atributos dir(bgui.FrameButton)
import menuPrincipal


#import bgui.bge_utils #corregir esto
"""
Probamos a importar la librería. Si la libreria es ejecutada desde otro módulo será ese módulo el encargado de proporcionarle al sistema la ruta donde debe encontrar la libreria bgui, si no la encuentra daremos una ruta que se unirá al sistema, con sys.path.append()
"""
try:
	from librerias.bgui_master import bgui # de esta forma podemos usar bgui.System, con la anterior no
	print ("bgui: \t cargada")
except ImportError:
	import os #funciones del sistema operativo
	os.chdir(bge.logic.expandPath('//')) # Cambiamos la ruta del os a donde tenemos nuestro .blend
	sys.path.append('') #Añadimos al sistema un directorio superiores, que es donde tenemos bgui
	print("Excepcion capturada")
	from librerias.bgui_master import bgui
except ImportError:
	print("fallo importando bgui")



class Creditos(bgui.bge_utils.Layout):
	def __init__(self, system, args):
		super().__init__(system, None)
		self.args = args

		#background black frame
		self.background = bgui.frame.Frame(system, name="raiz",size=[1, 1], pos=[0, 0]) #nombre a de ser raiz
		self.background.colors = [(0, 0, 0, 1) for i in range(4)] # the four corners black


		#open txt credits file
		path = bge.logic.expandPath("//")
		fichero = "creditos.txt"
		print(path+fichero)
		f = open(path+fichero, 'r')
		creditos = f.read()
		f.close()

		#print(creditos)
		x = .1
		#creditos = creditos.format('centered')
		print("creditos", creditos)

		#print on screen
		self.creditos = bgui.label.Label(self.background, name="creditos", text=creditos, font=None, pt_size=None, color=None, pos=[x,0])


		#self.creditos = bgui.text_block.TextBlock(self.background, name="creditos", text=creditos, font=None, pt_size=None, color=None, pos=[x,.5])
		#movement credits
		self.creditos.move([x,4],50000)
		

		self.back_button = bgui.frame_button.FrameButton(self.background, name="Volver", base_color=[1,1,1,.15], text='Volver',  size=[.15, .075], pos=[.8, .05])
		self.back_button.on_release = self.volver

	def volver(self, widget):
		self.system.load_layout(menuPrincipal.MenuPrincipal, self.args)
		#self.args["juego"].cambiarPantalla(menuPrincipal.MenuPrincipal, self.args)
