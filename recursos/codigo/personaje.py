# -*- coding: utf-8 -*-
#UTF 8

from collections import defaultdict
from functools import partial
import bge

import re #expresiones regulares

from bge import constraints

import json #Para cargar constraints ragdoll

from eventosManager import *
from acciones import *
#from debug import *

"""
def finished_cb(status, i):
	print("Library (%s) loaded in %.2fms." % (status.libraryName, status.timeTaken))
	print (i)
"""

"""
Clase personaje
"""
"""
Si un personaje tuviese que hablar se pondria una lista de sinonimos (tú, usted,...) y en el texto se usaria 
palabra.random(["tu","usted"]) + "que haces?" para que no dijese siempre lo mismo. Tambien se podria definir otra mas para que a 
ciertos personajes los llame de "tu" y a otros de "usted"
"""
#@debug
class Personaje():
	#gameObject se pasa con bge.logic.scene.addObject
	#de esta forma solo podemos añadir un personaje, no habra personajes duplicados
	def __init__(self, nombre): 
		self.nombre = nombre
		self.ragdoll = "//modelos/personajes/"+nombre+"_ragdoll_bones _list"
		#print("Objetos inactivos: ", bge.logic.getCurrentScene().objectsInactive)
		

		print("nombre personaje: " +nombre)
		print(bge.logic.getCurrentScene().objectsInactive)
		self.inactivo = bge.logic.getCurrentScene().objectsInactive[nombre+"Character"]

		self.armature = self.inactivo.children[self.nombre+"Armature"]	
		self.ragdoll_visible = False
		
		
		self.listaAcciones = {}
		self.elementosRagdoll = {}
		self.constraints = {}
		self.huesosGolpeadores = {}
		self.huesosImpacto = []
		
	"""
	Habria que crear un .blend con todos los personajes y darle tamaño para no tener que calcular la
	escala de cada personaje.
	"""
	def escala(self, escala=None):
		if(escala != None):
			self.escala = escala
			self.objeto.worldScale = escala 
		else:
			return self.escala
		
	def posX(self):
		#print("Posicion X personaje; ", self.objeto.worldPosition[0])
		return self.objeto.worldPosition[0]
	
	def posZ(self):
		#print("Posicion Y personaje; ", self.objeto.worldPosition[2])
		return self.objeto.worldPosition[2]
		

	def addNewAccion(self, accion, primerFrame, ultimoFrame, modo=bge.logic.KX_ACTION_MODE_LOOP, suavizado=2):
		if (accion not in self.listaAcciones):
			action = Accion(accion, primerFrame, ultimoFrame, modo, suavizado)
			self.listaAcciones[action.accion] = action
		else:
			print ("ERROR: ya hay  una accion con ese nombre para este personaje")
	
	def spawn(self, lugar):
		# bge.logic.getSceneList()["3D"].
		self.escena3D = bge.logic.getCurrentScene()#bge.logic.getSceneList()[bge.logic.getSceneList().index("3D")]
		self.objeto = self.escena3D.addObject(self.inactivo, bge.logic.getCurrentScene().objects[lugar])
		

		"""
		El armature solo esta en la escenas cuando añadamos el objeto 
		habria que calcularlo a partir de los children de self.objeto, para estar seguro en el caso
		de que personaje1 y personaje2 fueran el mismo
		"""
		self.armature = self.objeto.children[self.nombre+"Armature"]

		#Habria que comprobar que existe, pero como todos los personajes se suponen que van a tenerlo no lo hago
		
		print(self.armature)
		#
		#self.objeto.position = bge.logic.getCurrentScene().objects[lugar].position
		#self.crearConstraintsRagdoll()

		# Cargamos los datos serializados

		#Abrimos el json de nuestro personaje
		ruta= bge.logic.expandPath("//") + "modelos/personajes/"
		archivo = self.nombre+"_atributos.json"

		f = open(ruta+archivo, "r")
		serializeP1 = json.load(f)

		f.close()
		
		#self.huesosGolpeadores = serializeP1["huesosGolpeadores"]

		#self.escala([serializeP1["escalaX"], serializeP1["escalaY"], serializeP1["escalaZ"]])
		self.escala(bge.logic.getCurrentScene().objects[lugar].scaling)

		self.velocidadCorrer=serializeP1["velocidadCorrer"]*self.escala[0]
		self.velocidadAndar=serializeP1["velocidadAndar"]*self.escala[0]
		self.salto=[0,0,serializeP1["salto"]*self.escala[0]]
		
		
		#Cargamos todas las animaciones del json
		for i in serializeP1["animaciones"].keys():
			self.addNewAccion(i, serializeP1["animaciones"][i]["animacion"], serializeP1["animaciones"][i]["inicio"], serializeP1["animaciones"][i]["fin"], serializeP1["animaciones"][i]["modo"])

		self.huesosRagdoll = serializeP1["huesos"]

		#....
		self.huesosGolpeadores = serializeP1["huesosGolpeadores"]
		self.finGolpe = serializeP1["animaciones"]["golpe1"]["fin_golpe"]

		for i in self.huesosRagdoll:
			#if i not in self.huesosGolpeadores:
			self.huesosImpacto.append(i)

		print("Huesos impacto: ", self.huesosImpacto)

		""""""
		self.iniciaRagdoll()
		self.crearConstraintsRagdoll()
		self.desactivaRagdoll()
		
		
		#self.visibilidaRagdoll(True)
		print("spawn")


	def mover(self, movimiento):
		self.objeto.applyMovement(movimiento)

	def saltar(self, salto): #fuerza de salto por defecto
		self.objeto.applyForce(salto)
	
	def pararAccion(self):
		self.armature.stopAction()
	

	def reproducirAccion(self, accion, speed=1.0):
		self.armature.playAction(self.listaAcciones[accion].animacion, self.listaAcciones[accion].primerFrame, self.listaAcciones[accion].ultimoFrame, play_mode=self.listaAcciones[accion].modo, blendin=self.listaAcciones[accion].suavizado, speed = speed)

	def update(self):
		self.estadoCarga.update(self)
		
		
	
	def desactivaRagdoll(self, extremidades=None):
		#Primero emparentar sino los objetos rigid se emparejaran en lugar incorrecto
		#Hacer todo em el mismo bucle o fallará
		
		for hueso in self.elementosRagdoll:
			print(hueso)
			parent=self.armature.children["Empty"+hueso]
			self.elementosRagdoll[hueso].worldTransform = self.armature.children["Empty"+hueso].worldTransform
			self.elementosRagdoll[hueso].setParent(parent)
			self.armature.constraints[hueso+":Copy Transforms"].active = False
			#self.reorganizaRagdoll()
		#El codigo de abajo crea bug pies personajes
		"""
		#Hacer todo en el mismo bucle o fallará
		self.objeto.restoreDynamics()#
		for constraint in self.armature.constraints:
		
			
			hueso = constraint.__str__().split(":")[0]
			print(hueso) #equivale a toString()
			self.reorganizaRagdoll()#
			
			if (extremidades==None or hueso in extremidades):
				
				parent = self.armature.children["Empty"+hueso]
				self.elementosRagdoll[hueso].worldTransform = parent.worldTransform
				self.elementosRagdoll[hueso].setParent(parent,False,True) # 
				constraint.active = False #aqui esta el fallo
				
		"""

	"""
	#Funcion para reorganizar Ragdoll. 
	Esta funcion alinea de nuevos los objetos rigid que al activarse cambian ligeramente de sitio dando algunos fallos,
	no es la solución más optima, pero de forma temporal me vale
	"""
	def reorganizaRagdoll(self):
		for hueso in self.elementosRagdoll:
			self.elementosRagdoll[hueso].worldPosition = self.armature.children["Empty"+hueso].worldPosition

	def activaRagdoll(self, extremidades = None):
		#invisible y estatico
		
		print("activaRagdoll")
		
		self.objeto.suspendDynamics()
		
		for constraint in self.armature.constraints:

			"""
			a partir de : rShin:Copy Transforms'
			obtenemos rShin
			re.match(r'(.*?):.*', 'rShin:Copy Transforms').groups()[0]
			"""	

			hueso = constraint.__str__().split(":")[0]
			print(hueso) #equivale a toString()
			
			if (extremidades==None or hueso in extremidades):
				constraint.active = True
				self.elementosRagdoll[hueso].removeParent() #Esto segundo 
							
				
			self.reorganizaRagdoll()
			#hueso o hueso_aux???
				

	#modificar
	def ragdollPecho(self,personaje):
		personaje.activaRagdoll(["abdomen", "head", "rShldr", "rForeArm", "rHand", "lShldr", "lForeArm", "lHand"])
			
	
	def crearConstraintsRagdoll(self):
		#ruta = "/home/p9/Documentos/Proyecto/pfc2/recursos/modelos/personajes/" # bge.logic.currentPath("//"") + "modelos/personajes/" o algo asi
		
		ruta= bge.logic.expandPath("//") + "modelos/personajes/"
		archivo = self.nombre+"_constraints.json"

		f = open(ruta+archivo, "r")
		serialize = json.load(f)

		f.close()

		self.constraint = {}
		

		
		for hueso in serialize:

			print("Creando contraints de ", hueso)
			print("Posicion elemento ragdoll ", hueso, ": ", self.elementosRagdoll[hueso].worldPosition)
			physicsid = self.elementosRagdoll[hueso].getPhysicsId() #Tiene que estar aqui el fallo
			print("physicID1: "+self.elementosRagdoll[hueso].name)
			#print(dir(physicsid))
			hueso_target = serialize[hueso]["targetName"]
			hueso_target_rigid = serialize[hueso]["targetName"]+self.nombre
			print("Posicion elemento target ragdoll ", hueso_target, ": ", self.elementosRagdoll[hueso_target].worldPosition)
			print("hueso objetivo ", self.elementosRagdoll[hueso_target].name)
			physicsid2 = self.elementosRagdoll[hueso_target].getPhysicsId()
			print("physicID2")
			#print(dir(physicsid2))
			#print("objeto")
			#print(dir(self.elementosRagdoll[hueso]))
			
			# 12 = GENERIC_6DOF_CONSTRAINT is missing
			#bge.constraints.CONETWIST_CONSTRAINT
			#self.constraint[hueso] = constraints.createConstraint(physicsid, physicsid2,bge.constraints.CONETWIST_CONSTRAINT,.0,.0,.0) 


			constraint_type = 12
			#if (serialize[hueso]["pivot_type"] =="GENERIC_6_DOF")
				
			x = serialize[hueso]["pivot_x"]
			y = serialize[hueso]["pivot_y"]
			z = serialize[hueso]["pivot_z"]
			ax = serialize[hueso]["axis_x"]
			ay = serialize[hueso]["axis_y"]
			az = serialize[hueso]["axis_z"]


			aux = constraints.createConstraint(physicsid, physicsid2, constraint_type, x,y,z, ax, ay, az) 
			self.constraint[hueso] = aux
			print("constraint creada, acciones de constraint: ", bge.constraints.LINEHINGE_CONSTRAINT)
			#print(dir(self.constraint[hueso]))
			
			#http://www.blender.org/documentation/blender_python_api_2_72_1/bge.types.KX_ConstraintWrapper.html?highlight=setparam#bge.types.KX_ConstraintWrapper.setParam
			aux.setParam(0,.0,.0)#Limit the location in X axis
			print ("primer set param")
			
			aux.setParam(1, 0.0, 0.0) #Limit the location in Y axis
			aux.setParam(2, 0.0, 0.0) #Limit the location in Z axis

			aux.setParam(3, serialize[hueso]["limit_angle_min_x"], serialize[hueso][ "limit_angle_max_x"]) #Limit the rotation in X axis
			aux.setParam(4, serialize[hueso]["limit_angle_min_y"], serialize[hueso][ "limit_angle_max_y"]) #Limit the rotation in Y axis
			aux.setParam(5, serialize[hueso]["limit_angle_min_z"], serialize[hueso][ "limit_angle_max_z"]) #Limit the rotation in Z axis
			
			
			"""
			aux.setParam(6, 0.0, 0.0)
			aux.setParam(7, 0.0, 0.0)
			aux.setParam(8, 0.0, 0.0)
			
			aux.setParam(9, 0.0, 0.0)
			aux.setParam(10, 0.0, 0.0)
			aux.setParam(11, 0.0, 0.0)
			
			aux.setParam(12, 0.0, 0.0)
			aux.setParam(13, 0.0, 0.0)
			aux.setParam(14, 0.0, 0.0)
			
			aux.setParam(15, 0.0, 0.0)
			aux.setParam(16, 0.0, 0.0)
			aux.setParam(17, 0.0, 0.0)
			"""
			"""
			aux.setParam(1, 0.0, 0.0) #Limit the location in Y axis
			aux.setParam(2, 0.0, 0.0) #Limit the location in Z axis

			#Estos 3 parametros a 0 es como si el muñeco se quedara paralizado
			#aux.setParam(3, .0, .0) #Limit the rotation in X axis
			#aux.setParam(4, .0, .0) #Limit the rotation in Y axis
			#aux.setParam(5, .0, .0) #Limit the rotation in Z axis
			
			"""
			#Esto tengo que hacerlo despues de set param o habra un fallo de segmetacion
			parent = self.armature.children["Empty"+hueso]

			self.elementosRagdoll[hueso].setParent(parent,False,False)

			self.armature.constraints[hueso+":Copy Transforms"].target = self.elementosRagdoll[hueso]
			self.armature.constraints[hueso+":Copy Transforms"].active = False
			
			#self.armature.constraints[.target
			#print("Elemento cargado: ", self.elementosRagdoll[hueso])
			
			#self.armature.constraints[hueso].active = True
			#print("target anterior: ", self.armature.constraints[hueso+":Copy Transforms"].target)
			
			#print("target corregido: ", self.armature.constraints[hueso+":Copy Transforms"].target)
		print("armature constraints ", self.armature.constraints)

	def iniciaRagdoll(self):
		"""
		f = open(bge.logic.expandPath(self.ragdoll), 'r')
		"""
		ruta= bge.logic.expandPath("//") +"/modelos/personajes/" # bge.logic.currentPath("//"") + "modelos/personajes/" o algo asi
		archivo = self.nombre+"_constraints.json"
		
		g = open(ruta+archivo, "r")
		serialize = json.load(g)

		g.close()
		

		self.constraint = {}

		objectsInactive = bge.logic.getCurrentScene().objectsInactive
		print("objectsInactive: ", objectsInactive)
		for hueso in self.huesosRagdoll:
			#hueso = line[1:-2] # Eliminamos primer caracter ' y los dos ultimos ' y \n. Cuidado de meter un intro al final, sino no cogera el ultimo elemento
			print(hueso)
			parent = self.armature.children["Empty"+hueso]
			#self.objectsInactive = bge.logic.getCurrentScene().objectsInactive
			huesoRigid = hueso#+self.nombre
			rigid = objectsInactive[huesoRigid]
			self.elementosRagdoll[hueso] = self.escena3D.addObject(rigid, parent)

		#########Temporal mas adelante fallara
			#self.elementosRagdoll[hueso] = bge.logic.getCurrentScene().objects[hueso]
			
			######### constraints ########
			
			
			
		#f.close()
		
		print("ELEMENTOS CON addObject ",self.elementosRagdoll)
		
		self.visibilidaRagdoll(self.ragdoll_visible)

			
	def visibilidaRagdoll(self, visible=False):
		for i in self.elementosRagdoll:
			self.elementosRagdoll[i].visible = visible

			
	def update(self):
		

		pass
		
		#print("Rigid: ",self.elementosRagdoll["lShldr"].worldPosition)
		#print("Empty: ",self.armature.children["Empty"+"lShldr"].worldPosition)
		"""
		for i in self.elementosRagdoll:
			print("Posicion empty", i, ": ", self.armature.children["Empty"+i].worldPosition)
		"""
		

def visibilidaRagdoll (ragdoll):
	ragdoll.ragdoll_visible = not ragdoll.ragdoll_visible
	ragdoll.visibilidaRagdoll(ragdoll.ragdoll_visible)
	print("ragdoll visible: " ,ragdoll.ragdoll_visible)
	
def ragdollBrazoDer(personaje):
		personaje.activaRagdoll(["rShldr", "rForeArm", "rHand"])

def ragdollBrazoIzq(personaje):
		personaje.activaRagdoll(["lShldr", "lForeArm", "lHand"])

def ragdollPecho(personaje):
	personaje.activaRagdoll(["abdomen", "head", "rShldr", "rForeArm", "rHand", "lShldr", "lForeArm", "lHand"])