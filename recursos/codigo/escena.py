# -*- coding: utf-8 -*-

import sys
import bge
import os



os.chdir(bge.logic.expandPath('//')) # Es necesario para evitar errores!
# So we can find the bgui module
if 'codigo' not in sys.path:
	sys.path.append('codigo')

from cargaJuegoLucha import *
from debug import *


import bge

class Escena(bgui.bge_utils.System):
	"""
	def __init__(self):
		#self.escena = None
		self.blend = bge.logic.getCurrentScene()
	"""
	@postfuncion
	def __init__(self):
		super().__init__()
		self.inicializado = True
	#@postfuncion
	def cambiarEscena(self, escena,data):
		self.escena = escena(data)
		self.blend = bge.logic.getCurrentScene()

	def quitarEscena(self):
		self.escena = None
		self.blend = None

	def inicializado(self):
		return self.inicializado

	def addUI(self, ui,data=None):
		self.load_layout(ui, data)

	def update(self):
		self.escena.update()
