# -*- coding: utf-8 -*-

import sys
import bge
import os



os.chdir(bge.logic.expandPath('//')) # Es necesario para evitar errores!
# So we can find the bgui module
if 'codigo' not in sys.path:
	sys.path.append('codigo')


from librerias.bgui_master import bgui
from debug import *

class PantallaCargando(bgui.bge_utils.Layout):
	@postfuncion
	def __init__(self, system,data):
		super().__init__(system, None)
		#self.listaBotones = bgui.FrameButton(self, "Cargando", text="Cargando", size=[.4,.1],pos=[0.32,.45],base_color=[0,0,0,1])
		self.frame = bgui.frame.Frame(self, "frame")
		#self.fondo = bgui.image.Image(self, img="imagenes/"+"keyboard_angled_outline_T.png", name="fondo")
		
		self.frame.colors = [(1,1,1,1), (1,1,1,1), (1,1,1,1), (1,1,1,1)]
		self.iconoCargando = ["load1.png","load2.png","load3.png","load4.png"]
		self.imagenCarga = 0
		self.cargando = bgui.label.Label(self, "cargando",text="Cargando...",color=[0,0,0,1],pos=[.45,.1], pt_size=60)
		self.controles = bgui.label.Label(self, "controles",text="Controles",color=[0,0,0,1],pos=[.45,.9], pt_size=50)
		self.carga = bgui.image.Image(self, name="carga", img="imagenes/"+self.iconoCargando[self.imagenCarga],size=[.075, .1],pos=[.85,.1])
		#print("label")
		#self.juego = data["juego"]

		self.listaElementos= {}
		pos = [.075,.675]
		posImg = [pos[0]+.25,pos[1]-.025]
		tamLetra = 40
		colorLetra = [0,0,0,1]
		self.listaElementos["d"] = bgui.image.Image(self, name="D", img="imagenes/controles/"+"Keyboard_Black_Space.png",size=[.1, .1],pos=posImg)
		self.listaElementos["avanzar"] = bgui.label.Label(self,pt_size = tamLetra, color = colorLetra, name=_("Avanzar"),text=_("Avanzar"), pos=pos)
		
		pos = [pos[0],pos[1]-.075]
		posImg = [pos[0]+.25,pos[1]-.025]
		self.listaElementos["a"] = bgui.image.Image(self, name="A", img="imagenes/controles/"+"Keyboard_Black_A.png",size=[.045, .075],pos=posImg)
		self.listaElementos["retroceder"] = bgui.label.Label(self,pt_size = tamLetra, color = colorLetra, name=_("Retroceder"),text=_("Retroceder"), pos=pos)

		pos = [pos[0],pos[1]-.075]
		posImg = [pos[0]+.25,pos[1]-.025]
		self.listaElementos["espacio"] = bgui.image.Image(self, name="espacio", img="imagenes/controles/"+"Keyboard_Black_D.png",size=[.045, .075],pos=posImg)
		self.listaElementos["saltar"] = bgui.label.Label(self,pt_size = tamLetra, color = colorLetra, name=_("Saltar"),text=_("Saltar"), pos=pos)

		pos = [pos[0],pos[1]-.075]
		posImg = [pos[0]+.25,pos[1]-.025]
		self.listaElementos["j"] = bgui.image.Image(self, name="j", img="imagenes/controles/"+"Keyboard_Black_J.png",size=[.045, .075],pos=posImg)
		self.listaElementos["golpe"] = bgui.label.Label(self,pt_size = tamLetra, color = colorLetra, name=_("Golpear"),text=_("Golpear"), pos=pos)

		pos = [pos[0],pos[1]-.075]
		posImg = [pos[0]+.25,pos[1]-.025]
		self.listaElementos["k"] = bgui.image.Image(self, name="k", img="imagenes/controles/"+"Keyboard_Black_K.png",size=[.045, .075],pos=posImg)
		self.listaElementos["protegerse"] = bgui.label.Label(self,pt_size = tamLetra, color = colorLetra, name=_("Protegerse"),text=_("Protegerse"), pos=pos)



		posOk = [.75,.1]

		self.listaElementos["empezar"] = bgui.frame_button.FrameButton(self, name="empezar",text=_("Empezar"), pos=posOk, size=[.2,.1])
		self.listaElementos["empezar"].color=[.5, .5, .5,.65]
		self.listaElementos["empezar"].visible  = False
		self.listaElementos["empezar"].on_release = self.empezar

		self.contador = 0 #atributo de prueba
		#self.escena3D = self
		bge.logic.globalDict["juegoCargado"] = False
		bge.logic.globalDict["ko"] = False
		bge.logic.globalDict["pausa"] = False
		bge.logic.globalDict["cargado"] = False

		print("PantallaCargando().__init__()")
		print(bge.logic.getSceneList())
		# si ya se accedio restart sino add y en pause en lugar de end, suspend y un dict de control
		#if(bge.logic.getSceneList()[0].name == "UI"):
		##########################
		""""""
		#CUANDO BOTON
		""""""
		##########################
		bge.logic.addScene("3D", 0) #para que lo muestre debajo del UI
		#bge.logic.getSceneList()[0].resume() 
		#else:
			#bge.logic.getSceneList()[0].restart()
		print("PantallaCargando().__init__() escena 3D añadida")

	def empezar(self, widget):

		#no se puede aqui directamente porque este script se encuentra en otra escena
		bge.logic.globalDict["escena3Dy"]  = True


	def updateIconoCargando(self):
		if (self.imagenCarga < len(self.iconoCargando)-1):
			self.imagenCarga=self.imagenCarga+1
		else:
			self.imagenCarga=0
		self.carga.update_image("imagenes/"+self.iconoCargando[self.imagenCarga])

	def update(self,delta=10): # delta parámetro de actualización (visto en libgdx)
		self.contador = self.contador+1
		print(self.contador)

		if (bge.logic.globalDict["juegoCargado"]):
			self.listaElementos["empezar"].visible  = True
		#bge.logic.getCurrentScene().post_draw = [self.render]
		#Comprobación de que esta completa la carga asíncrona para pasar al juego?
		
		#actualizamos la ruedecita de carga
		self.updateIconoCargando()
		
		"""
		if(bge.logic.globalDict["juegoCargado"]):
			self.juego.cambiarPantalla(HudBatalla)
		"""