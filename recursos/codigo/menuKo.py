# -*- coding: utf-8 -*-

from codigo.menu import * #tengo meterlo asi
from codigo.menuPausa import *


from librerias.bgui_master import bgui


#import bge


class MenuKo(MenuInGame):
	def __init__(self, system,data, listaElementos = [_("Menu Principal"), _("Reiniciar"),_("Salir del Juego")]):
		super().__init__(system, data, listaElementos) #, data["juego"]
		#self.juego = data["juego"]
		self.sonidoMenu = Sonido("246349__borys-kozielski__sound-logo-koed.mp3")
		if (self.juego.musica == True):
			self.sonidoMenu.reproducir()
		else:
			self.sonidoMenu.reproducir(0)
		bge.logic.globalDict["ko"] = True
		print("menu KO")

		self.textoPausa = bgui.Label(self.raiz, text='KO', pos=[.5,.7],pt_size=120, options=bgui.BGUI_DEFAULT | bgui.BGUI_CENTERX)

		print("listaElementos[0]: ", listaElementos[0])
		self.botones.listaElementos[listaElementos[0]].on_release = self.menuPrincipal
		self.botones.listaElementos[listaElementos[1]].on_release = self.reiniciarJuego
		self.botones.listaElementos[listaElementos[2]].on_release = self.salirJuego

	def reiniciarJuego(self, widget):
		self.sonidoMenu.stop()
		super().reiniciarJuego(widget)
	
	def menuPrincipal(self, widget):
		self.sonidoMenu.stop()
		super().menuPrincipal(widget)