# -*- coding: utf-8 -*-

import bge
import os
import mathutils

from luchador import *
from luchadorBot import *
from acciones import *
from escenario import *
from hudBatalla import *
from menuKo import *
from menuPausa import *
from debug import *
from audio import *
#from debug import *

#@postfuncion
class JuegoLucha():
	@postfuncion
	def __init__(self, data):
		#http://www.blender.org/documentation/blender_python_api_2_67_release/bge.types.KX_LibLoadStatus.html
		"""
		print("Cargando Juego")
		print("Escenas, cargadas: ", bge.logic.getSceneList())
		print("Librerias blend: ",  bge.logic.LibList())
		"""
		bge.logic.globalDict["ko"] = False #Al empezar el juego estamos seguro nadie ko
		print("data: ", data)
		self.juego =bge.logic.globalDict["JuegoLucha"]["juego"]

		#Añadimos menu pausa, se crea oculto
		#juego = data["juego"]
		self.juego.cambiarPantalla(MenuPausa, {"juego":self.juego}) #crearlo dentro de juego lucha? para poder reiniciar despues de ko?	

		self.escena3D = data["escena"]
		bge.logic.globalDict["JuegoLucha"]["objeto"] = self
		self.data = data

		#Audio
		sonido = "100877__xythe__fight.wav"
		#sonido = "157846__darkmast49__fightscene.mp3"
		self.musica = Sonido("batalla/"+sonido)
		if (self.juego.musica==True):
			self.musica.playLoop(.25)
		else:
			self.musica.playLoop(0)
		
		self.fight =Sonido("fx/"+"276254__dynajinn__fight-voiceover-fight.wav")
		bge.logic.globalDict["musicaJuego"] = self.musica
		

		#print("atributos data JuegoLucha()", data)


		#self.hudBatalla = HudBatalla(bgui.bge_utils.System)
		#self.sistemaGUI = SistemaGUI(HudBatalla)

		#self.estadoKO = False #Control si luchador ko
		
		print("globalDict: ", bge.logic.globalDict)
		"""
		Cargamos Escenario
		"""
		self.escenario = Escenario(data["escenario"])
		#Probar
		#self.escenario.cargar()
		#print("Escena actual: ", bge.logic.getCurrentScene())
		self.escenario.calcularListaSpawn()
		self.escenario.calcularListasCamaras()
		#print(self.escenario.listaSpawn)
		#print(bge.logic.getCurrentScene().cameras)
		self.escenario.activarCamara("CameraJuegoLucha") #Cambiamos la camara activa
		#bge.logic.getCurrentScene().cameras["CameraJuegoLucha"].setViewport(0,0,bge.render.getWindowHeight, bge.render.getWindowWidth)
		#self.escenario.calcularLimites()
		#self.juego.cambiarPantalla(HudBatalla, data) #debajo de activar camara
		#self.juego.cambiarPantalla(HudBatalla)
		self.escena3D.addUI(HudBatalla)
		#print(bge.logic.LibList())
		#print(bge.logic.getSceneList())
		
		"""
		Personaje1 bot
		"""

		if(self.data["controlados"][0]):
			self.personaje1 = Luchador(data["personaje1"], self.escenario)
			self.personaje1.spawn(self.escenario.listaSpawn[1])
			"""
			Controles personaje 1 (Esto iria en LuchadorJugador, ya que LuchadorBot no lo tendria)
			"""
			
			self.personaje1.addMovimiento(bge.events.AKEY, bge.logic.KX_INPUT_ACTIVE,partial(retroceder, self.personaje1)) #cambiar por retroceder
			self.personaje1.eventos.addListener(bge.events.AKEY, bge.logic.KX_INPUT_JUST_RELEASED,partial(parar, self.personaje1)) #podria ser partial(self.personaje1.avanzar,...)

			self.personaje1.eventos.addListener(bge.events.DKEY, bge.logic.KX_INPUT_ACTIVE,partial(avanzar, self.personaje1)) #, [-self.personaje1.velocidad,0,0]
			self.personaje1.eventos.addListener(bge.events.DKEY, bge.logic.KX_INPUT_JUST_RELEASED,partial(parar, self.personaje1))

			self.personaje1.eventos.addListener(bge.events.KKEY, bge.logic.KX_INPUT_ACTIVE,partial(proteger, self.personaje1))
			self.personaje1.eventos.addListener(bge.events.KKEY, bge.logic.KX_INPUT_JUST_RELEASED,partial(parar, self.personaje1))
			
			

			self.personaje1.addMovimiento(bge.events.SPACEKEY,  bge.logic.KX_INPUT_JUST_ACTIVATED,partial(saltar, self.personaje1))
			#self.personaje1.addMovimiento(bge.events.SPACEKEY, bge.logic.KX_INPUT_JUST_RELEASED,partial(saltar, self.personaje1))

			self.personaje1.addMovimiento(bge.events.JKEY, bge.logic.KX_INPUT_JUST_ACTIVATED,partial(golpe1, self.personaje1))

			"""
			self.personaje1.eventos.addListener(bge.events.VKEY, bge.logic.KX_INPUT_JUST_RELEASED,partial(visibilidaRagdoll, self.personaje1))
			#Falta spawn de los elementos  del ragdoll
			self.personaje1.addMovimiento(bge.events.RKEY, bge.logic.KX_INPUT_JUST_RELEASED,partial(activaRagdoll, self.personaje1))
			self.personaje1.addMovimiento(bge.events.TKEY, bge.logic.KX_INPUT_JUST_RELEASED,partial(desactivaRagdoll, self.personaje1))
			self.personaje1.addMovimiento(bge.events.OKEY, bge.logic.KX_INPUT_JUST_RELEASED,partial(ragdollBrazoDer, self.personaje1))
			self.personaje1.addMovimiento(bge.events.IKEY, bge.logic.KX_INPUT_JUST_RELEASED,partial(ragdollBrazoIzq, self.personaje1))
			"""
		else: 
		#print("Creamos personaje")
			self.personaje1 = LuchadorBot(data["personaje1"], self.escenario)
			self.personaje1.spawn(self.escenario.listaSpawn[1])
				
		""""""
		
		"""
		Cargamos personaje 2
		"""
		#print("personaje2: ", personaje2)
		
		self.personaje2 = LuchadorBot(data["personaje2"], self.escenario)

		#print("Personaje2: ", personaje2, ". Direccion: ", self.personaje2)
		#print("Personaje2: ", personaje2, ". Armature: ", self.personaje2.armature)
		self.personaje2.spawn(self.escenario.listaSpawn[0])
		#print("Creacion juego finalizada")


		""""""
		"""
		Asignación de enemigos
		"""
		self.personaje1.enemigo(self.personaje2)
		self.personaje2.enemigo(self.personaje1)
		#print("juego lucha")

		print("Escenas, cargadas: ", bge.logic.getSceneList())
		for escena in bge.logic.getSceneList():
			print("objetos en ", escena, ": ", escena.objects)

		print("Escena actual: ", bge.logic.getSceneList())
	#toggle

		
		#self.juego.addPantalla(MenuPausa)
		#self.juego.layout.visible = False

		#self.fight.reproducir(1)
		self.controlEstado = 0

	
	def destroy(self):
		self.personaje1.objeto.endObject()
		self.personaje2.objeto.endObject()
	

	def update(self):
		#print(bge.logic.CONSTANT_TIMER) #siempre 13?
		#self.sistemaGUI.run(self.personaje1.vida/100,self.personaje2.vida/100)
		print("juegoLuchaupdate")
		if (self.controlEstado<2):
			self.controlEstado +=  1
		if (self.controlEstado==2):
			self.controlEstado +=  1
			if (self.juego.sonido==True):
				self.fight.reproducir(1)

		if ("reiniciar" in bge.logic.globalDict):
			#bge.logic.restartGame()
			#bge.logic.getCurrentScene().restart()

			
			self.personaje1.vida = 100
			self.personaje1.objeto.worldPosition =  bge.logic.getCurrentScene().objects[self.escenario.listaSpawn[1]].worldPosition
			self.personaje1.estado = LuchadorSpawn(self.personaje1)
			self.personaje1.reproducirAccion("idle")

			self.personaje2.vida = 100
			self.personaje2.objeto.worldPosition =  bge.logic.getCurrentScene().objects[self.escenario.listaSpawn[0]].worldPosition
			self.personaje2.estado = LuchadorSpawn(self.personaje2)
			self.personaje2.reproducirAccion("idle")

			bge.logic.globalDict["ko"] = False
			bge.logic.globalDict.pop("reiniciar")
			if (self.juego.sonido==True):
				self.fight.reproducir()
			else:
				self.fight.reproducir(0)

			#self.destroy()
			#self.escena3D.cambiarEscena(JuegoLucha, self.data)
			#CUIDADO CON EL ESCENARIO no se borro

		else:

			if(not bge.logic.globalDict["ko"]): #cambiar esto, por opcion con el vencedor estado parado
				
				if (self.personaje1.estado.nombre() == "KO" or self.personaje2.estado.nombre() == "KO"):
					print("alguien ko")
					bge.logic.globalDict["ko"] = True
					
					self.musica.stop()
					
					self.juego.cambiarPantalla(MenuKo, {"juego":self.juego})
					#self.data["juego"] = self.juego.system
					#self.juego = self.juego.system
				else:
					self.personaje1.update()


					print("personaje1 estado: ",self.personaje1.estado.nombre())
					
					#self.sistemaGUI.pantalla.update(self.personaje1.vida,self.personaje2.vida)
					#print("update personaje1")
					self.personaje2.update()
					print("personaje2 estado: ",self.personaje2.estado.nombre())
					#print("update personaje2")
					#layout!, poniendo pantalla falla
					self.escena3D.layout.update(self.personaje1.vida/100,self.personaje2.vida/100)

