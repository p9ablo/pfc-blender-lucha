# -*- coding: utf-8 -*-
""""
Error en poedit sino declaramos en el código que estamos en utf-8. Si lo declaramos podremos usar simbolos como á,é,... dentro de strings

Esta linea hay que usarla al menos con poedit en windows. Probar si en linux tambien

http://stackoverflow.com/questions/6289474/working-with-utf-8-encoding-in-python-source
http://stackoverflow.com/questions/4872007/where-does-this-come-from-coding-utf-8
"""
import bge

"""
Se ha añadido el parametro parte, por si dentro de una acción hay varias animaciones como puede ser en salto que serian 3 partes (saltar, en el aire, caer) seria de la siguiente forma

Accion("salto", 1, 5, parte=1,modo, suavizado)
Accion("salto", 6, 10, parte=2,modo, suavizado)
Accion("salto", 11, 15, parte=3,modo, suavizado)


Luego seria como si tuvieramos 3 animaciones distintas salto1, salto2, salto3

"""
class Accion:
	def __init__(self, accion, animacion, primerFrame, ultimoFrame, modo="KX_ACTION_MODE_LOOP", suavizado=1):

		self.animacion = animacion
		self.accion = accion
		self.primerFrame = primerFrame
		self.ultimoFrame = ultimoFrame
		
		self.suavizado = suavizado

		if(modo == "KX_ACTION_MODE_LOOP"):
			self.modo = bge.logic.KX_ACTION_MODE_LOOP
		elif(modo == "KX_ACTION_MODE_PLAY"):
			self.modo = bge.logic.KX_ACTION_MODE_PLAY
		else:
			print("Ningún modo seleccionado")
		

	#En python todas las variables son referencias
	



