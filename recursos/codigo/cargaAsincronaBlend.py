# -*- coding: utf-8 -*-
"""
Módulo para la gestión de librerías .blend
Módulo para la carga asíncrona
"""
import bge

import platform

from debug import *
from functools import partial
from juegoLucha import *

"""
lista con las librerías cargadas? 

Para cargas más rápidas se podría usar
bge.logic.loadGlobalDict() Loads bge.logic.globalDict from a file.
bge.logic.saveGlobalDict()

Realmente no haría falta una clase, salvo por mantener un estilo
"""
#@debug
@postfuncion
class LibreriaBlend():
	"""
	Cuidado escenarios y personajes que están en distintas rutas
	Se me ocurren 2 opciones:
	-Opción 1: lista
	
	-Opción 2: De forma manual como en juegoLucha cargando uno por uno y haciendo un update que cuando todos
				estén cargados, cambie la escena. comprobando con self.numeroBlends = 0
	"""
	def __init__(self): #escena no necesario
		self.listaBlends = []
		
		#self.nivel = nivel
		"""
		if (platform.system() == "Linux" ):
			for blend in bge.logic.LibList():
				bge.logic.LibFree(blend)
		"""
		self.numeroBlends = 3
		self.libreria = {}
		#self.cargado = False
		#Vector de 2 personajes
		
	#Método que pregunta continuamente si está cargado el .blend?
	def update(self):
		#Comprueba si listaBlends esta vacía, en caso de que lo éste invoca a cargaEscena
		print("Blends: ", self.numeroBlends, "Lista: ", len(self.listaBlends))
		#Implementarlo de forma dirigida a eventos
		#len(self.listaBlends) > 0 and 
		if (self.numeroBlends == 0): #Cuidado con esto  and self.cargado == False
			print("Carga finalizada")
			#IMPORTANTE REVISAR
			#self.nivel.juego.cambiarPantalla(JuegoLucha(self.nivel.juego, self.nivel.personaje1, self.nivel.personaje2, self.nivel.escenario)) #pasar parametros escenario, personaje1,...

			return True
		return False
	"""
	El escenario no se encuentra en una capa oculta, así que supongo que será lo ultimo que tengo que cargar.
	Lo suyo sería que cargara pero oculto, y lo mostrase cuando este todo listo. No idea pero se hace bien.

	Tal vez dejando mientras no esté todo terminado dejando las listas pre_draw y post_draw vacías
	http://www.blender.org/documentation/blender_python_api_2_69_1/bge.types.KX_Scene.html
	
	Cuidado porque libload carga los objetos en la escena actual, por lo tanto si cambiamos de escena ya
	no estarán los objetos que hayamos cargados, tanto los visibles como los inactivos.
	
	Hay 2 opciones para la carga asíncrona (Con la primera hace falta cambiar menos código, cargarScript): 
		-Todas en una misma escena, libLoad no daría problemas. Habría que cambiar el módulo a ejecutar
		en el controlador de python. Con lo que no se inicializaria bien lo que tuviera antes del update
		(comprobarlo)
		
		-2 escenas, como lo he hecho en principio, pero habría que tener el cuidado de cargar con libLoad
		los obejetos en la escena correcta. Supongo que habría que cambiar la escena activa
	"""
	
	def cargaEscenario(self, nombre):
		ruta = bge.logic.expandPath("//modelos/escenarios/" + nombre + ".blend") #Converts a blender internal path into a proper file system path.
		
		print(bge.logic.LibList())
		if(ruta not in bge.logic.LibList()):
			
			#http://glench.com/articles/python-async-callback.html
			#self.libreria[nombre] = 
			bge.logic.LibLoad(ruta, "Scene", load_actions=True, async=False) #Scene, Mesh o Action
			#self.libreria[nombre].onFinish = partial(self.cargado, ruta)
			#print("Cargando: ", self.libreria[self.numeroBlends-1])
		#else:
			self.numeroBlends -= 1

		print("cargado: "+ ruta)

	def cargaPersonaje(self, nombre):
		ruta = bge.logic.expandPath("//modelos/personajes/" + nombre + ".blend") #Converts a blender internal path into a proper file system path.
		if(ruta not in bge.logic.LibList()):
			
			#self.libreria[nombre] = 
			bge.logic.LibLoad(ruta, "Scene", load_actions=True, async=False)
			#self.libreria[nombre].onFinish = partial(self.cargado, ruta)
		else:
			self.numeroBlends -= 1
		print("cargado: "+ ruta)

			
			
	def cargaJuego(self, escenario, personaje1, personaje2):
		#self.numeroBlends = 3 #Si los 2 eligen mismo personaje no funcionaria
		self.escenario = escenario
		
		self.personaje1 = personaje1
		self.personaje2 = personaje2
		
		self.cargaEscenario(escenario)
		self.cargaPersonaje(personaje1)
		self.cargaPersonaje(personaje2)
		self.numeroBlends=0

	"""
	Cuidado status ha de ser el ultimo parametro
	Ejemplo de status:
	http://www.blender.org/documentation/blender_python_api_2_69_1/bge.types.KX_LibLoadStatus.html#bge.types.KX_LibLoadStatus
	"""
	def cargado(libreria, ruta, status=None):
		libreria.numeroBlends = libreria.numeroBlends - 1
		print("Cargado Ruta: %s" % status.libraryName, " numero blends: ", libreria.numeroBlends)
		#print("Objetos inactivos: ", bge.logic.getCurrentScene().objectsInactive)
		libreria.listaBlends.append(ruta) #Comprobar


	"""
	Empty juego está en la misma escena que el empty de carga pero a diferencia de éste se encuentra
	oculto, esto es así para que no se ejecute desde el incicio, asi lo ejecutamos solo cuando añadamos
	ese objeto a la escena.
	
	De esta forma solo necesitamos una escena en lugar de 2.
	"""
	#def iniciaJuego(self, empty="Juego"): #iniciar de otra forma
		#print(bge.logic.getCurrentScene().objectsInactive)
		#bge.logic.getCurrentScene().addObject(bge.logic.getCurrentScene().objectsInactive[empty],bge.logic.getCurrentController().owner)
		
		#print("objeto juego agregado")
"""
Mirar los apuntes de eventos para ver si puedo implementar		
def onFinish():
"""
