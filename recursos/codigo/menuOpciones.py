# -*- coding: utf-8 -*-

import bge
import os
import sys

import json

"""
Ruta
"""
os.chdir(bge.logic.expandPath('//')) # Cambiamos la ruta del os a donde tenemos nuestro .blend
if 'codigo' not in sys.path:
	sys.path.append('codigo') #para no tener que poner from codigo.menuPrincipal import *


from menu import *

from functools import * #partial
import gettext


cuatro3 = [[512,384], [640,480],[800,600],[1024,768]]
dieciseis9 = [[640,360],[854,480], [960,540],[1280,720],[1920,1080]]
aspecto = ["4:3","16:9"]
#lang = gettext.translation('menuPrincipal', localedir=directorio, languages=[data["idioma"]])
#lang.install()
#_ = lang.gettext
#siNo = [_("No"), _("Si")]

class MenuOpciones(Menu):
	def __init__(self, system, data):
		super().__init__(system)

		self.idioma = data["idioma"]
		self.juego = data["juego"]
		"""
		directorio = bge.logic.expandPath("//")+"traducciones" #Revisar esto
		lang = gettext.translation('menuPrincipal', localedir=directorio, languages=[data["idioma"]])
		lang.install()
		_ = lang.gettext
		"""
		self.siNo = [_("No"), _("Si")]
		self.opcionesDefecto()

		fondoRelativo="imagenes/menu.png"
		fondo = bge.logic.expandPath("//") + fondoRelativo

		self.listaElementos= {}

		self.fondoMenu = bgui.image.Image(self.raiz , fondo, fondo, size=[1, 1], pos=[0, 0], options = bgui.BGUI_DEFAULT|bgui.BGUI_CENTERX|bgui.BGUI_CACHE)
		size = [.4,.7]
		tamLetra = 50
		
		posOpciones = [.4, .8]
		self.listaElementos["opciones"] = bgui.label.Label(self.raiz,pt_size = tamLetra+30, name="opciones",text="Opciones", pos=posOpciones)
		
		if(aspecto[self.opciones["aspecto"]]=="16:9"):
			resolucion = dieciseis9[self.opciones["dieciseis9"]]
		else:
			resolucion = cuatro3[self.opciones["cuatro3"]]

		pos = [.075,.675]
		#A resolucion_menor y resolucion_mayor, añadir onClick y onHover
		self.listaElementos["resolucion"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name=_("Resolucion"),text="Resolucion", pos=pos)
		self.listaElementos["resolucion_menor"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="resolucion_menor",text="<", pos=[pos[0]+.3,pos[1]])
		self.listaElementos["resolucion_elegida"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="resolucion_elegida",text=(str(resolucion[0])+"x"+str(resolucion[1])), pos=[pos[0]+.4,pos[1]])
		self.listaElementos["resolucion_mayor"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="resolucion_mayor",text=">", pos=[pos[0]+.6,pos[1]])
		
		pos = [pos[0], pos[1]-.1]
		self.listaElementos["aspecto"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="aspecto",text=_("Aspecto"), pos=pos)
		self.listaElementos["aspecto_menor"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="aspecto_menor",text="<", pos=[pos[0]+.3,pos[1]])
		self.listaElementos["aspecto_elegida"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="aspecto_elegida",text=aspecto[self.opciones["aspecto"]], pos=[pos[0]+.025+.4,pos[1]])
		self.listaElementos["aspecto_mayor"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="aspecto_mayor",text=">", pos=[pos[0]+.6,pos[1]])
		
		pos = [pos[0], pos[1]-.1]
		self.listaElementos["pantallaCompleta"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="pantallaCompleta",text=_("Pantalla Completa"), pos=pos)
		self.listaElementos["pantallaCompleta_menor"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="pantallaCompleta_menor",text="<", pos=[pos[0]+.3,pos[1]])
		self.listaElementos["pantallaCompleta_elegida"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="pantallaCompleta_elegida",text=self.siNo[self.opciones["pantallaCompleta"]], pos=[pos[0]+.05+.4,pos[1]])
		self.listaElementos["pantallaCompleta_mayor"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="pantallaCompleta_mayor",text=">", pos=[pos[0]+.6,pos[1]])
		
		pos = [pos[0], pos[1]-.1]
		self.listaElementos["musica"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="musica",text=_("Musica"), pos=pos)
		self.listaElementos["musica_menor"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="musica_menor",text="<", pos=[pos[0]+.3,pos[1]])
		self.listaElementos["musica_elegida"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="musica_elegida",text=self.siNo[self.opciones["musica"]], pos=[pos[0]+.05+.4,pos[1]])
		self.listaElementos["musica_mayor"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="musica_mayor",text=">", pos=[pos[0]+.6,pos[1]])

		pos = [pos[0], pos[1]-.1]
		self.listaElementos["efectosSonido"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="efectosSonido",text=_("Efectos Sonido"), pos=pos)
		self.listaElementos["efectosSonido_menor"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="efectosSonido_menor",text="<", pos=[pos[0]+.3,pos[1]])
		self.listaElementos["efectosSonido_elegida"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="efectosSonido_elegida",text=self.siNo[self.opciones["efectosSonido"]], pos=[pos[0]+.05+.4,pos[1]])
		self.listaElementos["efectosSonido_mayor"] = bgui.label.Label(self.raiz,pt_size = tamLetra, name="efectosSonido_mayor",text=">", pos=[pos[0]+.6,pos[1]])

		posCancelar = [.075,.05]
		self.listaElementos["cancelar"] = bgui.frame_button.FrameButton(self.raiz, name="cancelar",text=_("Cancelar"), pos=posCancelar, size=[.2,.1])
		self.listaElementos["cancelar"].color=[1, 1, 1,.3]

		posOk = [.6,.05]
		self.listaElementos["ok"] = bgui.frame_button.FrameButton(self.raiz, name="ok",text=_("Aceptar"), pos=posOk, size=[.2,.1])
		self.listaElementos["ok"].color=[1, 1, 1,.3]
		#Botones
		self.listaElementos["cancelar"].on_release = self.cancelar
		self.listaElementos["ok"].on_release = self.ok

		self.listaElementos["resolucion_menor"].on_mouse_enter = partial(self.tamBoton,75)
		self.listaElementos["resolucion_menor"].on_mouse_exit = partial(self.tamBoton,50)

		self.listaElementos["resolucion_mayor"].on_mouse_enter = partial(self.tamBoton,75)
		self.listaElementos["resolucion_mayor"].on_mouse_exit = partial(self.tamBoton,50)

		self.listaElementos["resolucion_menor"].on_click = partial(self.cambiarValor,"resolucion",-1, self.listaElementos["resolucion_elegida"])
		self.listaElementos["resolucion_mayor"].on_click = partial(self.cambiarValor,"resolucion",+1, self.listaElementos["resolucion_elegida"])

		self.listaElementos["aspecto_menor"].on_mouse_enter = partial(self.tamBoton,75)
		self.listaElementos["aspecto_menor"].on_mouse_exit = partial(self.tamBoton,50)

		self.listaElementos["aspecto_mayor"].on_mouse_enter = partial(self.tamBoton,75)
		self.listaElementos["aspecto_mayor"].on_mouse_exit = partial(self.tamBoton,50)

		self.listaElementos["aspecto_menor"].on_click = partial(self.cambiarValor,"aspecto",-1, self.listaElementos["aspecto_elegida"])
		self.listaElementos["aspecto_mayor"].on_click = partial(self.cambiarValor,"aspecto",+1, self.listaElementos["aspecto_elegida"])

		self.listaElementos["pantallaCompleta_menor"].on_mouse_enter = partial(self.tamBoton,75)
		self.listaElementos["pantallaCompleta_menor"].on_mouse_exit = partial(self.tamBoton,50)

		self.listaElementos["pantallaCompleta_mayor"].on_mouse_enter = partial(self.tamBoton,75)
		self.listaElementos["pantallaCompleta_mayor"].on_mouse_exit = partial(self.tamBoton,50)

		self.listaElementos["pantallaCompleta_menor"].on_click = partial(self.cambiarValor,"pantallaCompleta",-1, self.listaElementos["pantallaCompleta_elegida"])
		self.listaElementos["pantallaCompleta_mayor"].on_click = partial(self.cambiarValor,"pantallaCompleta",+1, self.listaElementos["pantallaCompleta_elegida"])

		self.listaElementos["musica_menor"].on_mouse_enter = partial(self.tamBoton,75)
		self.listaElementos["musica_menor"].on_mouse_exit = partial(self.tamBoton,50)

		self.listaElementos["musica_mayor"].on_mouse_enter = partial(self.tamBoton,75)
		self.listaElementos["musica_mayor"].on_mouse_exit = partial(self.tamBoton,50)

		self.listaElementos["musica_menor"].on_click = partial(self.cambiarValor,"musica",-1, self.listaElementos["musica_elegida"])
		self.listaElementos["musica_mayor"].on_click = partial(self.cambiarValor,"musica",+1, self.listaElementos["musica_elegida"])

		self.listaElementos["efectosSonido_menor"].on_mouse_enter = partial(self.tamBoton,75)
		self.listaElementos["efectosSonido_menor"].on_mouse_exit = partial(self.tamBoton,50)

		self.listaElementos["efectosSonido_mayor"].on_mouse_enter = partial(self.tamBoton,75)
		self.listaElementos["efectosSonido_mayor"].on_mouse_exit = partial(self.tamBoton,50)

		self.listaElementos["efectosSonido_menor"].on_click = partial(self.cambiarValor,"efectosSonido",-1, self.listaElementos["efectosSonido_elegida"])
		self.listaElementos["efectosSonido_mayor"].on_click = partial(self.cambiarValor,"efectosSonido",+1, self.listaElementos["efectosSonido_elegida"])

		

	def cancelar(self, widget):
		self.system.load_layout(menuPrincipal.MenuPrincipal, {"idioma": self.idioma, "juego": self.juego})

	def ok(self,widget):
		#guardar cambios
		self.guardarOpciones()
		self.cancelar(widget)


	def tamBoton(self, tam, widget):
		widget.pt_size=tam

	def opcionesDefecto(self):
		#self.opciones ={"resolucion":[854,480], "cuatro3":1, "dieciseis9":1, "aspecto":0, "pantallaCompleta":0, "musica":1, "efectosSonido":1}
		self.opciones = self.juego.opciones
		self.guardarOpciones()

	def cambiarValor(self, opcion, operacion, otherWidget, widget):
		

		if (opcion=="resolucion"):
			if (self.opciones["aspecto"]==0):
				lista = cuatro3
				opcion = "cuatro3"
			else:
				lista = dieciseis9
				opcion = "dieciseis9"
		elif opcion == "aspecto" :
			lista = aspecto
		else :
			lista = self.siNo

		self.opciones[opcion]+=operacion

		if (self.opciones[opcion]<0):
			self.opciones[opcion] = len(lista)-1
		if (self.opciones[opcion]>=len(lista)):
			self.opciones[opcion] = 0

		if (opcion=="cuatro3"):
			self.opciones["resolucion"] = cuatro3[self.opciones["cuatro3"]]
			otherWidget.text = str(self.opciones["resolucion"][0]) + "x" + str(self.opciones["resolucion"][1])
		elif (opcion=="dieciseis9"):
			self.opciones["resolucion"] = dieciseis9[self.opciones["dieciseis9"]]
			otherWidget.text = str(self.opciones["resolucion"][0]) + "x" + str(self.opciones["resolucion"][1])
		else:
			otherWidget.text = lista[self.opciones[opcion]]
		
		self.juego.cambiarOpciones(self.opciones)
		#callback()


	def guardarOpciones(self):
		f = open('opciones.json', 'w')
		json.dump(self.opciones,f, indent=4)
		f.close()
