# -*- coding: utf-8 -*-

#UTF8
import bge
import os
import sys
import gettext

"""
Ruta
"""
os.chdir(bge.logic.expandPath('//')) # Cambiamos la ruta del os a donde tenemos nuestro .blend
if 'codigo' not in sys.path:
	sys.path.append('codigo') #para no tener que poner from codigo.menuPrincipal import *

from menuPrincipal import *
from menuEleccion import *
from menuIdioma import *


_ = None

class EscenaVacia():
	def update(self):
		pass

class Juego(bgui.bge_utils.System): #O debería llamarse Pantalla?
	def __init__(self):
		super().__init__('themes/default.cfg')
		self.pantalla=None

		self.resolucion = [854,480]
		self.musica = True
		self.sonido = True

	def setIdioma(self, idioma):
		self.idioma = idioma
		print(idioma)
		directorio = bge.logic.expandPath("//")+"traducciones/" #Revisar esto
		print("directorio traducciones:", directorio)
		lang = gettext.translation('traduccion', localedir=directorio, languages=[idioma]) #cuidado corchete en idiomas!!!
		lang.install()
		_ = lang.gettext
		
	def getIdioma(self):
		return self.idioma
	"""
	Implementando 2 métodos de este tipo. Una que borre lo que había en self.pantalla, por ejemplo para cuando se inicie el juego.
	Y la otra que no, por ejemplo para en el menu de seleccionar personaje pase al escenario y poder volver atrás.
	"""
	def cambiarPantalla(self, pantalla=None, data=None): #cargarPantalla es mas correcto
		self.load_layout(pantalla, data)


	def addPantalla(self, pantalla, data=None):
		self.add_overlay(pantalla,data)

	def eliminarPantalla(self):
		self.pantalla = None 
		self.load_layout(None)


	def update(self): #hacer una lista con los updates
		self.run()#run()

	def cargarOpciones(self):
		f = open('opciones.json', "r") #comprobar existe
		self.opciones = json.load(f)
		f.close()

	def cambiarOpciones(self, opciones=None):
		if (opciones!=None):
			self.opciones = opciones
		width = self.opciones["resolucion"][0]
		height = self.opciones["resolucion"][1]
		enable = self.opciones["pantallaCompleta"]
		bge.render.setWindowSize(width, height)
		bge.render.setFullScreen(enable)
		self.musica = self.opciones["musica"]
		self.sonido = self.opciones["efectosSonido"]
		#self.sonido = self.opciones["resolucion"][0]
		self.render()


"""
Meter el codigo de aqui en otro archivo .py... por ejemplo: pantallaJuego.py?
"""
juego = Juego()

juego.cargarOpciones()
juego.cambiarOpciones()


"""
En libgdx la pantalla cambia en el mismo momento que se crea la clase, por ejemplo
el objeto menuPrincipal no sé si sería necesario, por ejemplo dentro de MenuPrincipal se borraría también el self.pantalla actual
MenuPrincipal tiene que recibir juego, porque en algún momento dentro del menu principal se cambiará la pantalla del juego
"""
#pantallaInicial = SistemaGUI(MenuIdioma, juego)
juego.load_layout(MenuIdioma, {"juego":juego})


def update():
	#juego.run() #dentro de juego.update warning
	juego.update()
