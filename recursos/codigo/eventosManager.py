# -*- coding: utf-8 -*-

from collections import defaultdict
from functools import partial
from bge import logic, events


class EventManager:
    
    def __init__(self):
        self.events = defaultdict(set)
    
    def addListener(self, event, status, callback):
        self.events[event].add((status, callback))
    
    def update(self):
        events = logic.keyboard.active_events.copy()
        events.update(logic.mouse.active_events)
        
        for event, data in self.events.items():
            for (status, callback) in data:
                if events.get(event) == status:
                    callback()


class EventManagerEstado:
	def __init__(self):
		self.events = defaultdict(set)
		#self.personaje = personaje

	def addListener(self, event, callback):
        	self.events[event].add(callback)

	def update (self, objeto):
		events = objeto.estado

		for events in self.events.items():
			callback()
