# -*- coding: utf-8 -*-

import sys
import bge
import os
import math

#import struct #para usar dir y comprobar los atributos dir(bgui.FrameButton)
import menuPrincipal

from audio import *
from pantallaCargando import *
from eventosManager import *
import platform #to see if we are on linux or windows



#import bgui.bge_utils #corregir esto
"""
Probamos a importar la librería. Si la libreria es ejecutada desde otro módulo será ese módulo el encargado de proporcionarle al sistema la ruta donde debe encontrar la libreria bgui, si no la encuentra daremos una ruta que se unirá al sistema, con sys.path.append()
"""
try:
	from librerias.bgui_master import bgui # de esta forma podemos usar bgui.System, con la anterior no
	print ("bgui: \t cargada")
except ImportError:
	import os #funciones del sistema operativo
	os.chdir(bge.logic.expandPath('//')) # Cambiamos la ruta del os a donde tenemos nuestro .blend
	sys.path.append('') #Añadimos al sistema un directorio superiores, que es donde tenemos bgui
	print("Excepcion capturada")
	from librerias.bgui_master import bgui
except ImportError:
	print("fallo importando bgui")


class ElementoMenu():#Antes en vez de layout vacio
	def __init__(self, parent, nombreFrame, borde=0, aspecto=None, tamElem = [.4,.1], size=[1,1], pos=[0,0]): #z_index para visibilidad de capas
		self.parent = parent
		#Cada elemento es independiente y esta emparentado con la raíz
		print("frame", nombreFrame, size, pos)
		self.frame = bgui.frame.Frame(parent, nombreFrame, border=borde, aspect = aspecto, size=size, pos=pos) #The Widget class should not be used directly in a gui, but should instead be subclassed to create other widgets.
		print("frame visible? ", self.frame.visible)
		self.nombreFrame = nombreFrame
		self.anchoElemento = tamElem[0]
		self.altoElemento = tamElem[1]
		print("ancho, alto: ", self.anchoElemento, self.altoElemento)
		self.listaElementos={}
		
	"""
	¿Hacer que la posición de los 2 métodos siguientes sean respecto al widget en lugar de la pantalla completa?
	"""
	def anadirBoton(self, nombreBoton, pos, size = [.4,.1], color=[1, 1, 1,.3]):  #pt_size – the point size of the text to draw (defaults to 30 if None)
		if (nombreBoton not in self.listaElementos):#comprobar esto
			self.listaElementos[nombreBoton] = bgui.frame_button.FrameButton(self.frame, name=nombreBoton,text=nombreBoton, pos=pos)
		else:
			print("Error: Ya hay un botón con ese nombre")

	def anadirLabel(self, nombreBoton, pos, size = [.4,.1], color=[1, 1, 1,.3]):  #pt_size – the point size of the text to draw (defaults to 30 if None)
		if (nombreBoton not in self.listaElementos):#comprobar esto
			self.listaElementos[nombreBoton] = bgui.label.Label(self.frame, name=nombreBoton,text=nombreBoton, size=size, pos=pos)
		else:
			print("Error: Ya hay un botón con ese nombre")

	def anadirBoton(self, nombreBoton, pos, size = [.4,.1], color=[1, 1, 1,.3]):  #pt_size – the point size of the text to draw (defaults to 30 if None)
		if (nombreBoton not in self.listaElementos):#comprobar esto
			self.listaElementos[nombreBoton] = bgui.frame_button.FrameButton(self.frame, name=nombreBoton, base_color=color,text=nombreBoton, size=size, pos=pos)
		else:
			print("Error: Ya hay un botón con ese nombre")

	def ocultarElementos(self):
		for i in self.listaElementos: #recorrer diccionario es distinto de una lista
			self.listaElementos[i].visible = False
			
	def mostrarElementos(self):
		for i in self.listaElementos: #recorrer diccionario es distinto de una lista
			self.listaElementos[i].visible = True

	#Creo que no llego a usar esta función nunca
	def anadirImagenBoton(self, nombre, pos, ruta="imagenes/fotoPesonaje/", extension=".png"): #añadir parámetro ruta /imagenes/banderas, y parámetro extension .jpg, .png
		if (nombre not in self.listaElementos):
			"""Problema con posElemento"""
			print("args: ",nombre, pos, ruta+nombre+extension)
			
			self.listaElementos[nombre] = bgui.image_button.ImageButton(self.frame, name=nombre, default_image=[ruta+nombre+extension, 0,0,1,1], aspect=16/9,size=[self.anchoElemento, self.altoElemento], pos=pos) #cambiar carpeta
			
		else:
			print("Error: Ya hay un botón con ese nombre")
			
	
class MosaicoElementoMenu(ElementoMenu): #La herencia al reves
	"""
	Eliminar size y que se calcule solo? tener cuidado con los widget con size diferente de 1:1 lo mismo se distorsiona
	Hay que saber o pasar el numElementos o listaElementos
	"""
	def __init__(self, parent, nombreFrame, guia="centro", tamElem = [.4,.1],borde=0, filas=None,columnas=1, espacioElementos=.025, centro = [.5,.5], size=[1,1], pos=[0,0]): #Elemento guia? ["centro", esqIzqSup, esqDerSup, esqIzqInf..]
		super().__init__(parent, nombreFrame, aspecto=16/9, borde=borde, tamElem = tamElem,size=[1,1], pos=[0,0])

		self.espacioElementos = espacioElementos
		#self.listaElementos={}
		self.posActual = 0
		self.filas = filas
		self.columnas = columnas
		self.guia = guia
		self.centro = centro
		self.posActual = 0
		
		self.primerElementoX = pos[0] #+ size[0]
		self.primerElementoY = pos[1] #+ size[1]
		"""
		Implementar mosaico aqui...
		"""
		
	def calcularPosicionElementos(self):
		if(self.guia=="centro"): 
			#self.centro = centro
			if (self.columnas%2==0):
				self.primerElementoX = self.centro[0]- (self.espacioElementos/2+(self.anchoElemento+self.espacioElementos)*int(self.columnas/2)) #Calculado despejando centro de la ecc de abajo
			else:
				self.primerElementoX = self.centro[0]-(self.anchoElemento/2 +(self.anchoElemento+self.espacioElementos)*int((self.columnas-1)/2)) #Calculado despejando centro de la ecc de abajo
				
			if(self.filas%2==0):
				self.primerElementoY = self.centro[1]+ (self.espacioElementos/2 + (self.altoElemento+self.espacioElementos)*int(self.filas/2))-self.altoElemento
				print("filas222", self.centro[1])
				
			else:
				self.primerElementoY = self.centro[1]+ (self.altoElemento/2+ (self.altoElemento+self.espacioElementos)*int((self.filas-1)/2))-self.altoElemento
			print("x, y: ",self.primerElementoX,self.primerElementoY)

		
	"""
	MosaicoElementos que herede de ElementoMenu. MosaicoElementosMenu.. y métodos para posActual, siguiente, columnas...
	"""
	def mosaicoElementos(self, listaElementos, funcion, **kwargs):
		if (self.filas ==None): 
			self.filas = math.ceil(len(listaElementos) / self.columnas) #Hay que redondear hacia arriba
		print("filas: ", self.filas)
		self.numElementos = len(listaElementos)
		self.calcularPosicionElementos()

		print("primerX= ", self.centro[0]-self.anchoElemento*(3/2)-self.espacioElementos)
		print("primerY= ", self.centro[0]+self.altoElemento*(3/2)-self.espacioElementos)
		for elemento in listaElementos:
			print("elemento: ", elemento)
			funcion(elemento, **kwargs)
		
	
	#Añadir opcion centrar ultima fila? aqui o arriba?
	def elementoSiguiente(self): 
		#int(3/2) para truncar la division
		"""
		if(self.filas%2==0):
			sigX = self.primerElementoX+int((self.columnas-1)/self.posActual)*(self.espacioElementos+self.anchoElemento)
			sigY = self.primerElementoY-int((self.filas-1)/self.posActual)*(self.espacioElementos+self.altoElemento)
		"""
		"""
		if(self.posActual==1):
			sigX=self.primerElementoX
			sigY=self.primerElementoY
		else:
			sigX = self.primerElementoX+math.ceil(self.posActual%self.columnas)*(self.espacioElementos+self.anchoElemento)
			sigY = self.primerElementoY-math.ceil(self.posActual%self.filas)*(self.espacioElementos+self.altoElemento)
		"""
		sigX = self.primerElementoX+math.ceil(self.posActual%self.columnas)*(self.espacioElementos+self.anchoElemento)
		sigY = self.primerElementoY-math.ceil(self.posActual%self.filas)*(self.espacioElementos+self.altoElemento)
		
		return [sigX, sigY]

	def anadirBoton(self, nombreBoton, size = [.4,.1], color=[1, 1, 1,.3]):  #pt_size – the point size of the text to draw (defaults to 30 if None)
		if self.numElementos>0:
			self.numElementos = self.numElementos-1
			sig = self.elementoSiguiente()
			
			super().anadirBoton(nombreBoton, sig, size, color)
			self.posActual += 1
		else:
			print("ERROR: demasiados elementos en el mosaico")
			
	#Creo que no llego a usar esta función nunca
	def anadirImagenBoton(self, nombre, ruta, extension=".png"): #añadir parámetro ruta /imagenes/banderas, y parámetro extension .jpg, .png
		if self.numElementos>0:
			self.numElementos = self.numElementos-1
			sig = self.elementoSiguiente()
			
			print(sig)
			print(self.posActual)
			super().anadirImagenBoton(nombre, pos=sig, ruta=ruta)
			self.posActual += 1
		else:
			print("ERROR: demasiados elementos en el mosaico")



"""
 A partir de aqui usamos las funciones y clases de bgui
Class Menu, que reciba una lista con nombre de botones
bgui.system: The main gui system. Add widgets to this and then call the render() method draw the gui.
En python el primer argumento de cualquier función es siempre self.
Nuestra clase Menu hereda de bgui.System
Un elemento puede ser un boton, imagen, etiqueta...
"""
class Menu(bgui.bge_utils.Layout): #Antes bgui.bgui.System
	"""
	A subclass to handle our game specific GUI
	"""
	def __init__(self, system, aspecto=None, espacioElementos=.12): #0.12 es porcentaje
		# Initialize the system con un tema
		#data = [juego, espacioElementos]
		print("juego: ", system)
		super().__init__(system, None)
		#¿Añadir un widget al que emparentamos todo para cuando queramos ocultar?
		#Hacer varios widgets? Fondo, en el caso eleccion personajes fondo, eleccionPersonaje, botones...
		#self.fondo = bgui.widget.Widget(self, "fondo", size=[1, 1])
		
		#self.system = system #Porque no lo pilla?
		
		self.raiz = bgui.frame.Frame(system, "raiz", aspect=aspecto, size=[1, 1])
		
		#self.layout = self.raiz #PROVISIONAL
		
		#Mostramos el ratón
		bge.logic.mouse.visible = True
		
		#self.juego = system

	
	def salirJuego(self, widget): #Pone un dispositivo no cerrado
		#self.exit()
		#for escena in bge.logic.getSceneList():
		#bge.logic.getCurrentScene().end()
		print("saliendo")
		bge.logic.endGame()
		
	#Este método dejan de ser necesarios
	def cargarEscena(self, widget, escena):	 #widget?
		bge.logic.getCurrentScene().replace(escena) #No escena cargando
	
	
	
#Menú por defecto del juego lucha. La imagen de fondo debe ir antes de crear los botones ya que si 
#no la dibujaría por encima de los botones
class MenuJuegoLuchaDefecto(Menu):
	def __init__(self, system, listaElementos, fondo=None, centro=[.55,.45]):
		super().__init__(system)				
		print("juego(defecto): ", system)
		self.sonidoOverBoton = Sonido("jumpland44100.mp3")
		self.sonidoClickBoton = Sonido("MenuChoice.mp3")
		class Aux():
			def __init__(self):
				self.listaElementos = {}
		self.botones = ElementoMenu(self.raiz, "botones")
		
		#Menú fondo
		#texco = None. The UV texture coordinates to use for the image. Para atlas de imágenes
		#Comentar esta linea
		if (fondo != None):
			self.fondoMenu = bgui.image.Image(self.raiz , fondo, fondo, size=[1, 1], pos=[0, 0], options = bgui.BGUI_DEFAULT|bgui.BGUI_CENTERX|bgui.BGUI_CACHE)
		"""
		print("creando mosaico")
		self.botones = MosaicoElementoMenu(self.raiz, "botones", centro=centro)
		#self.menuBotonesCentrado(self.botones, listaElementos)
		
		self.botones.mosaicoElementos(listaElementos, self.botones.anadirBoton)#, size=[.7,.2])#
		"""

		#map?
		pos = [centro[0]-.4/2, centro[1]+(len(listaElementos)/2) * .125 ]
		#print("elementos: ",self.botones.listaElementos)
		for boton in listaElementos:
			pos[1]-=.125
			self.botones.listaElementos[boton] = bgui.frame_button.FrameButton(self.raiz, name=boton,text=boton, pos=pos, size=[.4,.1])
			self.botones.listaElementos[boton].color=[1, 1, 1,.3]
			self.botones.listaElementos[boton].on_click = self.sonidoClick #sonido al clickar
			self.botones.listaElementos[boton].on_mouse_enter = self.sonidoHover #sonido al pasar por encima
	
	
	def sonidoHover(self, widget): #sonidoHover
		if (self.system.sonido==True):
			self.sonidoOverBoton.reproducir()
		else:
			self.sonidoOverBoton.reproducir(0)
		#self.handle2 = self.device.play(self.sonidoOverBoton) #me puedo ahorrar self.handle2?

	def sonidoClick(self, widget): #sonidoClick
		if (self.system.sonido==True):
			self.sonidoClickBoton.reproducir()
		else:
			self.sonidoClickBoton.reproducir(0)
		#self.handle3 = self.device.play(self.sonidoClickBoton)
		
	def exit(self):
		self.sonidoOverBoton.stop()
		self.sonidoClickBoton.stop()

class MenuInGame(MenuJuegoLuchaDefecto):
	def __init__(self, system, data, listaElementos, fondo=None, centro=[.5,.5]):
		super().__init__(system, listaElementos, fondo=fondo, centro=centro)
		#Para que al pulsar INTRO se pause
		self.eventos = EventManager()
		self.eventos.addListener(bge.events.ENTERKEY, bge.logic.KX_INPUT_JUST_ACTIVATED, self.pausa)
		
		self.juego = data["juego"]
		self.escena = bge.logic.getSceneList()[0] #[bge.logic.getSceneList().index("3D")] # bge.logic.getCurrentScene()

	def pausa(self, widget=None):#pulsando enter no requiere widget
		bge.logic.globalDict["pausa"] = not bge.logic.globalDict["pausa"]
		print("lista esecenas momento pausa: ", bge.logic.getSceneList())

		
		print("juego pausado: ", bge.logic.globalDict["pausa"], "escena actual: ", self.escena)
		
		if(not bge.logic.globalDict["pausa"]):
			if (self.juego.musica == True):
				bge.logic.globalDict["musicaJuego"].sonido.volume = .25
			else:
				bge.logic.globalDict["musicaJuego"].sonido.volume = 0
			self.escena.resume() 
			self.ocultar()

			
		else:
			if (self.juego.musica == True):
				bge.logic.globalDict["musicaJuego"].sonido.volume = .05
			else:
				bge.logic.globalDict["musicaJuego"].sonido.volume = 0
			self.escena.suspend()
			self.mostrar()
		
		print("pausa: ", bge.logic.globalDict["pausa"])

	def reiniciarJuego(self, widget):
		#bge.logic.getCurrentScene().restart()
		
		bge.logic.globalDict["pausa"] = False
		#bge.logic.globalDict["musicaJuego"].stop()
		self.ocultar()
		#self.escena.restart() 
		#CUIDADO CON EL ESCENARIO no se borro
		bge.logic.globalDict["reiniciar"] = True
		#self.pausa()
		self.escena.resume() 
		

	def ocultar(self):
		"""
		print("parent menu pausa: ", self.raiz.parent)
		print("children menu pausa: ", self.raiz.children)
		for i in self.raiz.children:
			i.visible=False
		"""
		self.botones.ocultarElementos()
		self.textoPausa.visible = False

	def mostrar(self):
		self.botones.mostrarElementos()
		self.textoPausa.visible = True

	def menuPrincipal(self, widget):
		bge.logic.globalDict["musicaJuego"].stop()
		#acabar escena 3D
		print("menu pausa, escena princpal: ", bge.logic.getCurrentScene())
		print("Librerias cargadas: ", bge.logic.LibList())
		#bge.logic.globalDict["JuegoLucha"]["escena"].quitarEscena()
		#bge.logic.globalDict["JuegoLucha"]["escena"].cambiarEscena(CargaJuegoLucha,bge.logic.globalDict["JuegoLucha"])
		bge.logic.globalDict["JuegoLucha"]["escena"].inicializado = False
		bge.logic.globalDict["JuegoLucha"]["objeto"].destroy()
		#bge.logic.getCurrentScene().replace('3D')
		#bge.logic.globalDict["juegoCargado"] = False
		juego = bge.logic.globalDict["JuegoLucha"]["juego"]
		bge.logic.globalDict.pop("JuegoLucha")
		bge.logic.globalDict.pop("ko")
		bge.logic.globalDict.pop("pausa")
		bge.logic.globalDict.pop("juegoCargado")

		
		

		self.escena.end() # en lugar de acabar aqui mandar señal a juego lucha y acabarlo alli
		#bge.logic.globalDict["JuegoLucha"]["escena"].blend.end()
		
		print("SO: " + platform.system())

		"""
		if (platform.system() == "Linux" ):
			for blend in bge.logic.LibList():
				bge.logic.LibFree(blend)#Esta linea falla en w64 bits v2.70
		"""
		
		"""
		for blend in bge.logic.LibList():
			bge.logic.LibFree(blend)
		"""
		#idioma almacenarlo en dict global o en atributo de juego
		juego.cambiarPantalla(menuPrincipal.MenuPrincipal, {"juego": juego, "idioma":juego.getIdioma()})


	def update(self):
		print("update menu pausa")
		self.eventos.update()
	
