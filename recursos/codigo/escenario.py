# -*- coding: utf-8 -*-

import bge

import re #expresiones regulares

class Escenario:
	def __init__(self, escenario):
		self.nombre = escenario
		self.listaSpawn = []
		self.listaCamaras = []
		self.escena = bge.logic.getCurrentScene() #wasteland
		self.muroDer = bge.logic.getCurrentScene().objects["muroDer"]
		print("muroDer: ", self.muroDer)
		self.muroIzq = bge.logic.getCurrentScene().objects["muroIzq"]
		

	def calcularListaSpawn(self):
		#busca en los objetos dos los que empiecen por Spawn+...
		#print(bge.logic.getCurrentScene().objects)
		#print("Calculando lista spawn")
		patron = re.compile(".*spawn.*")

		for i in range (len(bge.logic.getCurrentScene().objects)):
			#print(bge.logic.getCurrentScene().objects[i].name)
			#print(i)
			if patron.search(bge.logic.getCurrentScene().objects[i].name):
				#print(bge.logic.getCurrentScene().objects[i].name)
				self.listaSpawn.insert(len(self.listaSpawn), self.escena.objects[i].name)

	def calcularListasCamaras(self):
		patron = re.compile(".*Camera.*")

		for i in range (len(bge.logic.getCurrentScene().objects)):
			if patron.search(bge.logic.getCurrentScene().objects[i].name):
				self.listaCamaras.insert(len(self.listaCamaras), self.escena.objects[i].name)	
		print(self.listaCamaras)
	def calcularLimites(self):
		#self.limiteDer = self.escena.objects["limiteDer"].worldPosition[0]
		#self.limiteIzq =self.escena.objects["limiteIzq"].worldPosition[0]
		pass

	def imprimeListaSpawn(self):
		print("Lista spawn: ", self.listaSpawn)


	def activarCamara(self, nombreCamara):
		#bge.logic.getCurrentScene().active_camera=bge.logic.getCurrentScene().cameras[nombreCamara]
		print(self.escena)
		self.escena.active_camera = self.escena.cameras[nombreCamara]

