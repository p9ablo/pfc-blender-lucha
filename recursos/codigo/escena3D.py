# -*- coding: utf-8 -*-

import sys
import bge
import os



os.chdir(bge.logic.expandPath('//')) # Es necesario para evitar errores!
# So we can find the bgui module
if 'codigo' not in sys.path:
	sys.path.append('codigo')

from cargaJuegoLucha import *
from debug import *
from escena import *


import bge

"""
lucha = JuegoLucha(bge.logic.globalDict["JuegoLucha"])




escena3D = Escena()
bge.logic.globalDict["JuegoLucha"]["escena"] = escena3D
escena3D.cambiarEscena(CargaJuegoLucha,bge.logic.globalDict["JuegoLucha"])
"""
#control=False # en dict global de blender

print("blend escena3D cargado")
"""
update de esta manera o la comentada sino falla cuando eliminamos una escena y 
queremos volver a cargarla.
Al importar modulo solo se ejecuta una vez
"""

def update(cont):
	"""

	own = cont.owner
	mouse = bge.logic.mouse

	if 'sys' not in own:
		# Create our system and show the mouse
		own['sys'] = MySys()
		mouse.visible = True

	else:
		own['sys'].main()
	
	"""

	own = cont.owner
	try:
		escena3D = own['scene']
	except KeyError:
		escena3D = own['scene'] = Escena()
		bge.logic.globalDict["JuegoLucha"]["escena"] = escena3D
		escena3D.cambiarEscena(CargaJuegoLucha,bge.logic.globalDict["JuegoLucha"])
		#own['update'] =True

	escena3D.update()
	
