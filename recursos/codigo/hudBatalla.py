# -*- coding: utf-8 -*-

import sys
import bge
import os
import math

#import struct #para usar dir y comprobar los atributos dir(bgui.FrameButton)

from audio import *
from pantallaCargando import *
from eventosManager import *

#import bgui.bge_utils #corregir esto
"""
Probamos a importar la librería. Si la libreria es ejecutada desde otro módulo será ese módulo el encargado de proporcionarle al sistema la ruta donde debe encontrar la libreria bgui, si no la encuentra daremos una ruta que se unirá al sistema, con sys.path.append()
"""
try:
	from librerias.bgui_master import bgui # de esta forma podemos usar bgui.System, con la anterior no
	print ("bgui: \t cargada")
except ImportError:
	import os #funciones del sistema operativo
	os.chdir(bge.logic.expandPath('//')) # Cambiamos la ruta del os a donde tenemos nuestro .blend
	sys.path.append('') #Añadimos al sistema un directorio superiores, que es donde tenemos bgui
	print("Excepcion capturada")
	from librerias.bgui_master import bgui
except ImportError:
	print("fallo importando bgui")


class BarraVida():
	def __init__(self, parent, name, pos): #No necesario parent en esta clase
		self.parent = parent
		self.vida = 100
		directorio_imagen = "imagenes/hud/" 
		imagen_vida = "barra_vida"
		imagen_rectangulo = "rectangulo_vida"
		extension=".png"
		
		self.imagen_vida = directorio_imagen + imagen_vida + extension
		self.imagen_rectangulo = directorio_imagen + imagen_rectangulo + extension #recuadro
		
		#self.bgui_imagen_vida = bgui.image.Image(self.parent, self.imagen_vida, name=name+"v",  options = bgui.BGUI_DEFAULT|bgui.BGUI_CENTERX|bgui.BGUI_CACHE, aspect=0, pos=pos)
		#self.bgui_imagen_rectangulo = bgui.image.Image(self.parent, self.imagen_rectangulo,  options = bgui.BGUI_DEFAULT|bgui.BGUI_CENTERX|bgui.BGUI_CACHE,name=name+"r", aspect=0, pos=pos, size=[.1, .1])
		
		
		
class BarraEnergia():
	def __init__(self, parent, name, pos):
		self.parent = parent
		self.energia = 0
		
		
		directorio_blend = bge.logic.expandPath('//')
		directorio_imagen = "imagenes/hud/" 
		imagen_energia = "barra_energia"
		extension=".png"
		self.imagen_energia = directorio_blend+directorio_imagen + imagen_energia + extension
		
		print(self.imagen_energia)
		
		self.frame = bgui.frame.Frame(parent, name+"_frame", pos=pos)
		
		self.bgui_imagen_energia = bgui.image.Image(self.frame, self.imagen_energia)
		
		
class HudPersonaje():
	def __init__(self, parent, name, pos):
		#print("posssssssssssssssssssssssssssssssssssss ",pos[0])
		#pos[0]
		#pos[1]+=.1
		self.energia = BarraEnergia(parent, name=name, pos=pos)
		#self.vida = BarraVida(parent, name=name, pos=pos)
		
		
class HudBatalla(bgui.bge_utils.Layout):
	def __init__(self, system, pos1=[.0,.9], pos2=[.66,.9]):
		super().__init__(system, None)
		self.raiz = bgui.frame.Frame(system, "raiz", pos=[0,0], size=[1, 1])
		self.vida_inicial = 0.39
		parent = self.raiz
		
		#self.personaje1 = HudPersonaje(parent, name="per1", pos=pos1) 
		#self.personje2 = HudPersonaje(parent, name="per2", pos=pos2)
		
		directorio_blend = bge.logic.expandPath('//')
		directorio_imagen = "imagenes/hud/" 
		imagen_energia = "barra_energia"
		extension=".png"
		imagen_energia = directorio_blend+directorio_imagen + imagen_energia + extension
		
		imagen_vida = "barra_vida"
		imagen_rectangulo = "rectangulo_vida"
		
		imagen_vida = directorio_imagen + imagen_vida + extension
		imagen_rectangulo = directorio_imagen + imagen_rectangulo + extension
		
		

		print("vida inicial: ", self.vida_inicial)
		#hacer que cuando pulse + aumente tamaño, - disminuya y flechas mueva poss
		#1163 x 67
		self.vida_1 = bgui.image.Image(self.raiz, imagen_vida, pos=[.044,1-.1], size=[self.vida_inicial,.03])	# aspect=1163/67
		self.vida_2 = bgui.image.Image(self.raiz, imagen_vida, pos=[.54+.019,.9], size=[self.vida_inicial,.03])
		
		
		# 1339 x 262
		self.rectangulo_1 = bgui.image.Image(self.raiz, imagen_rectangulo, pos=[.025,.85], size=[.45,.1])		
		self.rectangulo_2 = bgui.image.Image(self.raiz, imagen_rectangulo, pos=[.54,.85], size=[.45,.1])
		
		self.energia_1 = bgui.image.Image(self.raiz, imagen_energia, pos=[.0325,.84], size=[.4,.04])		
		self.energia_2 = bgui.image.Image(self.raiz, imagen_energia, pos=[.54+.01,.84], size=[.4,.04])
		
		posicion = bge.logic.mouse.position
		
		"""
		self.mouse = bgui.label.Label(self.raiz, text=posicion.__str__())
		self.ancho = bgui.label.Label(self.raiz, text="Ancho: "+self.vida_1.size[0].__str__(), pos=[0,.05])
		self.alto = bgui.label.Label(self.raiz, text="Alto: "+self.vida_1.size[1].__str__(), pos=[0,.1])
		self.x = bgui.label.Label(self.raiz, text="X: "+self.vida_1.position[0].__str__(), pos=[0,.15])
		self.y = bgui.label.Label(self.raiz, text="Y: "+self.vida_1.position[1].__str__(), pos=[0,.2])
		"""
		self.eventos = EventManager()
		
		self.eventos.addListener(bge.events.PADPLUSKEY, bge.logic.KX_INPUT_ACTIVE, partial(aumentarAncho, self.vida_1))
		self.eventos.addListener( bge.events.UPARROWKEY, bge.logic.KX_INPUT_ACTIVE, partial(subirElemento, self.vida_1))


	#pasar parametros con la vida?
	def update(self, vida1=1, vida2=1):
		# aqui no entra
		#posicion = bge.logic.mouse.position
		#self.mouse.text = posicion.__str__()

		print("estado ko: ", bge.logic.globalDict["ko"])
		if (not bge.logic.globalDict["ko"]): #si un personaje ko no se puede pausar
			self.eventos.update()

		
		print("vida inicialaaa: ", self.vida_inicial)
		self.vida_1.size = [self.vida_inicial*vida1,.03]
		self.vida_2.size = [self.vida_inicial*vida2,.03]
		
		
		#print ("eeeeeeeeeeeeeeeeeeeee")
		 
	#def printDebug(self, elemento):
		



def aumentarAncho(elemento):
	elemento.size[0] += 0.1
	print ("aumentando ancho")
	elemento.text = "Ancho: "+elemento.size[0].__str__()
	
def subirElemento(elemento):
	elemento.position[0] += 0.1
	print ("aumentando ancho")
	elemento.move(elemento.position, 1)
	elemento.text = "Ancho: "+elemento.position[0].__str__()
	

