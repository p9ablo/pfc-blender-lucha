# -*- coding: utf-8 -*-

import bge
import sys
#https://wiki.python.org/moin/PythonDecoratorLibrary#Controllable_DIY_debug


def debug(funcion):
	import time
	def wrapper(*args, **kwargs):
		print("\n###################### \n")
		print("Nombre de la función: ", funcion.__name__) #sys.stderr no sé usarlo
		print("Argumentos: ", args, kwargs)
		print("Comentarios: ", funcion.__doc__)
		t = time.clock()
		f_result = funcion(*args, **kwargs)
		print("Función ", funcion.__name__,  "ejecutada correctamente en: ", time.clock()-t)
		print("Valor devuelto: ", f_result)

		return f_result
	return wrapper

def prefuncion(funcion):
	def wrapper(*args, **kwargs):
		print("\n###################### \n")
		print("Estado actual del motor (Game engine)")
		print(funcion.__name__, args)
		print("Listas de librerias: ", bge.logic.LibList())
		print("Escenas, cargadas: ", bge.logic.getSceneList())
		print("Escena actual: ", bge.logic.getCurrentScene())
		print("Camaras escena actual", bge.logic.getCurrentScene().cameras)
		print("Objetos activos: ", bge.logic.getCurrentScene().objects)
		print("Objetos inactivos: ", bge.logic.getCurrentScene().objectsInactive)
		print("globalDict: ", bge.logic.globalDict)
		print("controlador: ", bge.logic.getCurrentController())
		f_result = funcion(*args, **kwargs)

		return f_result
	return wrapper

def postfuncion(funcion):
	def wrapper(*args, **kwargs):
		print("\n###################### \n")
		print("Estado actual del motor (Game engine)")
		print(funcion.__name__, args)
		f_result = funcion(*args, **kwargs)
		print("Listas de librerias: ", bge.logic.LibList())
		print("Escenas, cargadas: ", bge.logic.getSceneList())
		print("Escena actual: ", bge.logic.getCurrentScene())
		print("Camaras escena actual", bge.logic.getCurrentScene().cameras)
		print("Objetos activos: ", bge.logic.getCurrentScene().objects)
		print("Objetos inactivos: ", bge.logic.getCurrentScene().objectsInactive)
		print("globalDict: ", bge.logic.globalDict)
		print("controlador: ", bge.logic.getCurrentController())
		return f_result
	return wrapper
"""
def ticRate(funcion, rate):
	def wrapper(*args, **kwargs):
		print("LogicTicRate cambiado a: ", rate)
		bge.logic.setLogicTicRate(rate)
		f_result = funcion(*args, **kwargs)
		
		return f_result
	return wrapper
"""
#http://stackoverflow.com/questions/15624801/passing-a-parameter-to-the-decorator-in-python	
import functools

def ticRate(rate):
	def dec(funcion):
		@functools.wraps(funcion)
		def wrapper(*args, **kwargs):
		#return "<%s> %s </%s>" % (tag, f0(*args, **kwargs), tag)
			print("LogicTicRate cambiado a: ", rate)
			bge.logic.setLogicTicRate(rate)
			f_result = funcion(*args, **kwargs)
			return f_result
		return wrapper
	return dec

