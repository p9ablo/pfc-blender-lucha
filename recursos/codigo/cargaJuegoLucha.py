# -*- coding: utf-8 -*-

import sys
import bge
import os



os.chdir(bge.logic.expandPath('//')) # Es necesario para evitar errores!
# So we can find the bgui module
if 'codigo' not in sys.path:
	sys.path.append('codigo')

from cargaAsincronaBlend import * #comprobar que importa los blends

from juegoLucha import *



class CargaJuegoLucha():
	@postfuncion
	def __init__(self, data):
		print("CargaJuegoLucha()")
		self.juego = data["juego"]

		self.data = data
		
		self.personaje1 = data["personaje1"]
		self.personaje2 = data["personaje2"]
		self.escenario = data["escenario"]
		self.escena = data["escena"]
		"""
		##################
		Cargamos el Juego
		##################
		
		De alguna forma hay que pasarle a juego, los personajes y escenario de esta clase.
		Para meter porcentaje de carga mirar el tamaño de cada blend, y aumentar el porcentaje de forma
		proporcional a su tamaño a medida que carga. Para esto habria que hacer una clase o función que lo
		vaya controlando.
		"""
		self.cargaJuego = LibreriaBlend() #pasarle self, para dentro hacer self.juego.cambiarPantalla(JuegoLucha(self.juego))?

		self.cargaJuego.cargaJuego(self.escenario, self.personaje1, self.personaje2)
		"""
		self.cargaJuego.cargaEscenario(self.escenario)
		print("bien carga escenario")
		self.cargaJuego.cargaPersonaje(self.personaje1)
		print("bien carga personaje")
		self.cargaJuego.cargaPersonaje(self.personaje2) 
		"""
		#bge.logic.addScene(self.escenario)
		self.init =False

		bge.logic.globalDict["escena3Dy"] = False
		self.control = True
		


	def update(self):
		print("cargaJuegoLuchaUpdate")
		#print("escenario in list? ", self.escenario.nombre,  self.escenario.nombre in bge.logic.getSceneList())
		print("Escenas, cargadas: ", bge.logic.getSceneList())
		print("cargaJuegoLucha.py:update() cargado?", self.cargaJuego.update())
		if (bge.logic.globalDict["escena3Dy"] == False):# and self.escenario.nombre in bge.logic.getSceneList()): #cambiar
			#self.juego.cambiarPantalla(JuegoLucha(self.juego, self.personaje1, self.personaje2, self.escenario)) #pasar parametros escenario, personaj
			#self.juego.cambiarPantalla(JuegoLucha, {"juego":self.juego, "personaje1":self.personaje1, "personaje2":self.personaje2, "escenario":self.escenario}) #pasar parametros escenario, personaj
			#aux = JuegoLucha(self.juego,self.personaje1,self.personaje2,self.escenario)
			
			if(self.cargaJuego.update() == True and self.init ==False):
				self.init =True
				
				
				bge.logic.globalDict["juegoCargado"] = True #reaprovechado para terminarlo

				print("cargando escenas")
				#probar que pasaria al reves
				self.escena.cambiarEscena(JuegoLucha, self.data)

				#Cambiaremos de escena en la pantalla cargando
				#bge.logic.globalDict["escena3Dy"] = self.escena
				#bge.logic.globalDict["escena3DyData"] = self.data
				#self.escena.cambiarEscena(JuegoLucha, self.data)
				#self.juego.eliminarPantalla()
				
		else:
			if(self.control):
				self.escena.cambiarEscena(JuegoLucha, self.data)
				bge.logic.globalDict["escena3Dy"] == False
				self.control = False
		#self.juego.layout.visible = False
			
			
			#print("tipo objeto: ",self.juego.pantalla)
			
			#self.juego.cargarEscena(JuegoLucha,{"juego":self.juego,"personaje1":self.personaje1,"personaje2":self.personaje2,"escenario":self.escenario})
			#self.juego.eliminarPantalla()
		"""
		if(self.cargaJuego.update() == True): #Eliminar esto y simplificar
			#self.__del__() #Comprobar si funciona #eliminado
			print("bgui destruido")
			#Esto dejará de ser necesario, ya que solo habrá un empty
			#bge.logic.getCurrentScene().objects["Cargando"].endObject()#bge.logic.getCurrentController().owner.endObject() 
		"""