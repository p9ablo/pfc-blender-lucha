# -*- coding: utf-8 -*-

from audio import *
from menu import *
from debug import *


from menuEleccion import *
from menuOpciones import *
from creditos import *


import bge
import platform #to see if we are on linux or windows
"""
IDIOMA = 'EN'

directorio = bge.logic.expandPath("//")+"traducciones" #Revisar esto
lang = gettext.translation('menuPrincipal', localedir=directorio, languages=[IDIOMA])
lang.install()
_ = lang.gettext
"""
"""
Mirar las opciones de los widgets
http://bgui.readthedocs.org/en/latest/api/widget.html 
#bgui.widget.Widget
"""

class MenuPrincipal(MenuJuegoLuchaDefecto):
	@postfuncion
	def __init__(self, system, data, fondo="imagenes/menu.png"):
		#data = juego, idioma

		
		self.idioma = data["idioma"]#bge.logic.globalDict["Idioma"] #data["Idioma"]
		self.juego = data["juego"]
		listaBotones=[_("Un Jugador"), _("Opciones"), _("Créditos"), _("Salir")]

		
		#De esta forma no lo pilla poedit
		"""
		for x in range(0, len(listaBotones)):
			listaBotones[x] = _(listaBotones[x])
		"""

		super().__init__(system, listaBotones, fondo) #super.__init__(self, system, juego, listaBotones, fondo)	

		#juego.cambiarPantalla(menuPrincipal) #cuidado esto no se hace aqui
		"""
		libgdx hace esto. (oculta la pantalla anterior)
		Sets the current screen. Screen.hide() is called on any old screen, and Screen.show() is called on the new screen, if any.
		"""

		#####################
		#	Música de fondo	#
		#####################
		print("antes fallo")

		if (self.juego.musica==True):
			self.sonidoMenu = Sonido("Bouncy_0.mp3")
			self.sonidoMenu.reproducir()

		#Operaciones de los botones
		#cambiar por un método, ya que puede ser listaBotones, listaElementos
		self.botones.listaElementos[listaBotones[3]].on_release = self.salirJuego #acción al levantar al dado

		#CORREGIR 
		#a la pantalla de menuEleccion no llega, con la pantallaCargando ningún problema. Problema que el update hace post_draw
		self.botones.listaElementos[listaBotones[0]].on_release = self.menuEleccion#self.crearJuego #cambiar nombre por cargarJuego

		self.botones.listaElementos[listaBotones[1]].on_release = self.menuOpciones

		self.botones.listaElementos[listaBotones[2]].on_release = self.creditos
		print(bge.logic.LibList())

		print("SO: " + platform.system())
		"""
		if (platform.system() == "Linux" ):
			for blend in bge.logic.LibList():
				bge.logic.LibFree(blend)#Esta linea falla en w64 bits v2.70

		"""
		
		print("antes fallo 2")

	def menuEleccion(self, widget): #Cuidado hay que poner widget // aun falla handle mouse que se queda abierto del update del menu anterior
		#print("entra en menuEleccion")
		if (self.juego.musica==True):
			self.sonidoMenu.stop()
		self.sonidoOverBoton.stop()
		self.sonidoClickBoton.stop()
		# sustituir los stop por __del__?
		print("System: ", self.system)
		#self.juego.cambiarPantalla(MenuEleccion(self.system, self.juego)) #No cambia la pantalla, solamente cambia el layout del system
		#self.system.load_layout(MenuEleccion, {"juego": self.juego, "idioma":self.idioma})
		self.system.cambiarPantalla(MenuEleccion, {"idioma":self.idioma, "juego": self.juego})
	def menuOpciones(self, widget): #Cuidado hay que poner widget // aun falla handle mouse que se queda abierto del update del menu anterior
		#print("entra en menuEleccion")
		if (self.juego.musica==True):
			self.sonidoMenu.stop()
		self.sonidoOverBoton.stop()
		self.sonidoClickBoton.stop()
		# sustituir los stop por __del__?
		print("System: ", self.system)
		#self.juego.cambiarPantalla(MenuEleccion(self.system, self.juego)) #No cambia la pantalla, solamente cambia el layout del system
		#self.system.load_layout(MenuEleccion, {"juego": self.juego, "idioma":self.idioma})
		self.system.cambiarPantalla(MenuOpciones, {"idioma":self.idioma, "juego": self.juego})

	def creditos(self, widget):
		print("creditos")
		if (self.juego.musica==True):
			self.sonidoMenu.stop()
		self.sonidoOverBoton.stop()
		self.sonidoClickBoton.stop()
		#bge.logic.addScene("Creditos", 0)
		self.system.cambiarPantalla(Creditos, {"idioma":self.idioma, "juego": self.juego})
	"""
	def update(self):
		#print("MenuPrincipal update")
		pass
	"""
	def exit(self):
		super().exit()
		if (self.juego.musica==True):
			self.sonidoMenu.stop()
