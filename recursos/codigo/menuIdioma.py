# -*- coding: utf-8 -*-

from menu import *
from menuPrincipal import *


import bge
from functools import * #partial

#banderas=["España", "Reino Unido", "Alemania", "Francia", "Italia"]

class MenuIdioma(Menu):#banderas=da
	def __init__(self, system, data,banderas=["spain", "united_kingdom", "germany", "france", "italy"]):
		print("sistema: ", system)
		super().__init__(system)
		self.juego = data["juego"]
		self.banderas = MosaicoElementoMenu(self.raiz, "banderas", tamElem=[.2,.2], centro=[.5,.5], columnas=3)
		ruta = bge.logic.expandPath("//")+"imagenes/paises/"
		self.banderas.mosaicoElementos(banderas, self.banderas.anadirImagenBoton, ruta=ruta)
		
		for x in range(0, 5):
			self.banderas.listaElementos[banderas[x]].on_release = partial(self.menuPrincipal, banderas[x])
			self.banderas.listaElementos[banderas[x]].on_mouse_enter = partial(self.tamBandera,banderas[x],[.21,.21])
			self.banderas.listaElementos[banderas[x]].on_mouse_exit = partial(self.tamBandera,banderas[x],[.2,.2])
		
	def menuPrincipal(self, idiom, widget):
		carpetaIdioma = {"spain": "es", "united_kingdom": "en", "germany": "de", "france": "fr", "italy": "it"}
		print("juegooo: ", self.system, "widget: ", widget, "idioma: ", idiom, "carpeta idioma: ", carpetaIdioma[idiom])
		
		self.juego.setIdioma(carpetaIdioma[idiom])
		bge.logic.globalDict["Idioma"] = self.juego.getIdioma()
		self.juego.cambiarPantalla(MenuPrincipal, {"juego": self.juego, "idioma":self.system.getIdioma()})
		#self.system.load_layout(MenuPrincipal, {"juego": self.juego, "idioma":carpetaIdioma[idiom]})

	def tamBandera(self, x,tam, widget):
		self.banderas.listaElementos[x].size=tam