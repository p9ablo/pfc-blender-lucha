# -*- coding: utf-8 -*-
import random

from luchador import *
from luchadorEstadoIA import *

"""
Código a mejorar:

	# self.establecerEstado(... esta feo mejor self.establecerEstado(
"""
AVANZAR = "correr"
SALTO1 = "agacharse"
SALTO2 = "salto2"
SALTO3 = "salto3"
INICIO_GOLPE1 = "inicio_golpe1"
GOLPE1 = "golpe1"
FIN_GOLPE1 = "fin_golpe1"
INICIO_GOLPE2 = "inicio_golpe2"
GOLPE2 = "golpe2"
FIN_GOLPE2 = "fin_golpe2"
RETROCEDER = "retroceder" #añadir animacion retroceder
PARADO = "idle" #cambiar parado por esperando
PROTEGERSE = "protegerse" # que solo se pueda mientras retrocede?
GOLPEADO = "Hurt1"
KO = "Death"
DESPROTEGERSE = "desprotegerse"
GOLPEADO_PROTEGIDO = "protegerse_impactado"
GOLPE_SALTANDO = "salto_golpe2"

class LuchadorEstado():
	def __init__(self, personaje):
		self.personaje = personaje
		self.huesosGolpeador = []
		self.controlEstado = False # Variable de auxiliar para controlar el estado, no se usa siempre. Borrar y ponerla solo donde se use?

	def establecerEstado(self, estado):#borrarlo de Luchador
		#self.estado.__del__()#supongo que hay que borrar el estado anterior? Creo que no hace falta destructores en python
		self.personaje.estado = estado #hacer esto con una funcion

	def avanzar(self):
		pass
	def saltar(self):
		pass

	def proteger(self):
		pass
	def golpeado(self, protegido=False):
		self.establecerEstado(LuchadorGolpeado(self.personaje, protegido))
	

	def retroceder(self):
		pass

	def nombre(self): #COMPROBAR
		return self.__class__.__name__[len("Luchador"):] #quitar la parte LuchadorSaltando -> dejarlo como saltando. len("Luchador") = 8 letras

	def update(self):
		pass
	#creo un método llamado ia donde se implementa la ia del bot?
	def updateIA(self):
		#print("IA: ", self.personaje)
		#print("Armature: ", self.personaje.armature)
		pass
	"""
	Hacer que todos estos estados devuelvan false, hacer una clase ia asociada luchador bot?
	"""
	def golpe1(self):
		pass

	def parar(self):
		self.establecerEstado(LuchadorParado(self.personaje)) #Esta feo mejor self.establecerEstado(
		
	
class LuchadorSpawn(LuchadorEstado):

	def update(self):
		self.establecerEstado(LuchadorParado(self.personaje))
			


class LuchadorParado(LuchadorEstado):
	def __init__(self, personaje):
		super().__init__(personaje)
		self.personaje.pararAccion()
		self.personaje.reproducirAccion(PARADO)
		if (bge.logic.globalDict["JuegoLucha"]["juego"].musica == True):
			self.personaje.sonidoAndar.reproducir(1)
		else:
			self.personaje.sonidoAndar.reproducir(0)
		self.tiempoParado = 0

	def avanzar(self):
		self.establecerEstado(LuchadorAvanzando(self.personaje))

	def retroceder(self):
		self.establecerEstado(LuchadorRetrocediendo(self.personaje))

	def proteger(self):
		self.establecerEstado(LuchadorProtegiendose(self.personaje))

	def saltar(self):
		self.establecerEstado(LuchadorSaltando(self.personaje))

	def golpe1(self):
		self.establecerEstado(LuchadorGolpeando(self.personaje))

	def updateIA(self):
		parado(self)



class LuchadorAvanzando(LuchadorEstado):
	def avanzar(self):
		self.personaje.mover([self.personaje.direccionAvanzar()*self.personaje.velocidadCorrer,0,0]) #CAMBIAR ESTO
		self.personaje.reproducirAccion(AVANZAR) #No va "" porque esta definido arriba
		#reproducirAccion(self.personaje, "avanzar")

	def golpe1(self):
		self.establecerEstado(LuchadorGolpeando(self.personaje))
		
	def parar(self):
		self.establecerEstado(LuchadorParado(self.personaje))
	
	def saltar(self):
		self.establecerEstado(LuchadorSaltandoAvanzando(self.personaje))

	#Comprobar Orientacion del personaje para saber si tiene que mirar limite izq o der
	def update(self):
		if(round(self.personaje.armature.getActionFrame())==3 or round(self.personaje.armature.getActionFrame())==31):
			if (bge.logic.globalDict["JuegoLucha"]["juego"].musica == True):
				self.personaje.sonidoAndar.reproducir(1)
			else:
				self.personaje.sonidoAndar.reproducir(0)
		if (not self.personaje.capazAvanzar()):
			self.parar()
			
	
	def updateIA(self):
		avanzando(self)


class LuchadorRetrocediendo(LuchadorEstado):
	def retroceder(self):
		self.personaje.mover([self.personaje.direccionRetroceder()*self.personaje.velocidadAndar,0,0])
		self.personaje.reproducirAccion(RETROCEDER)
		self.establecerEstado(LuchadorRetrocediendoRapido(self.personaje))
		
	def saltar(self):
		self.establecerEstado(LuchadorSaltandoRetrocediendo(self.personaje))

	def parar(self):
		self.establecerEstado(LuchadorParado(self.personaje))	
	def proteger(self):
		self.establecerEstado(LuchadorProtegiendose(self.personaje))
	
	def update(self):
		if(round(self.personaje.armature.getActionFrame())==3 or round(self.personaje.armature.getActionFrame())==31):
			if (bge.logic.globalDict["JuegoLucha"]["juego"].musica == True):
				self.personaje.sonidoAndar.reproducir(1)
			else:
				self.personaje.sonidoAndar.reproducir(0)
		if (self.personaje.capazRetroceder()==False):
			self.parar()
			
	def updateIA(self):
		retrocediendo(self)

class LuchadorRetrocediendoRapido(LuchadorEstado):
	def retroceder(self):
		self.personaje.mover([self.personaje.direccionRetroceder()*self.personaje.velocidadAndar*5,0,0])
		self.personaje.reproducirAccion(RETROCEDER, speed=2.0)
		
	def saltar(self):
		self.establecerEstado(LuchadorSaltandoRetrocediendo(self.personaje))
	def parar(self):
		self.establecerEstado(LuchadorParado(self.personaje))	
	def proteger(self):
		self.establecerEstado(LuchadorProtegiendose(self.personaje))
	
	def update(self):
		if(round(self.personaje.armature.getActionFrame())==3 or round(self.personaje.armature.getActionFrame())==31):
			if (bge.logic.globalDict["JuegoLucha"]["juego"].musica == True):
				self.personaje.sonidoAndar.reproducir(1)
			else:
				self.personaje.sonidoAndar.reproducir(0)
		if (self.personaje.capazRetroceder()==False):
			self.parar()
			
	def updateIA(self):
		retrocediendo(self)
	

"""
A la hora de saltar un personaje tiene 3 animaciones, coger impulso, saltar y caer (esta ultima es la misma que la primera pero reproducida inversamente).
Al pusar espacio se crea salto y reproduce animación de agachar, al soltarlo, ejecuta la funcion saltar() y reproduce la animación de irse hacia arriba, en update cuando toca suelo reproduce inversamente la primera animación
"""
class LuchadorSaltando(LuchadorEstado):	
	def __init__(self, personaje, impulso=[0,0,0]):
		super().__init__(personaje)
		self.personaje.objeto.applyForce(impulso)
		self.personaje.reproducirAccion(SALTO1)
		self.impulso = impulso
		self.saltando = False
		self.enAire = False
		self.cayendo = False

	def avanzar(self):	
		self.personaje.mover([self.personaje.direccionAvanzar()*self.personaje.velocidadCorrer,0,0])
	
	def retroceder(self):	
		self.personaje.mover([self.personaje.direccionRetroceder()*self.personaje.velocidadCorrer,0,0])
	

	def saltar(self):
		if (self.saltando==False):
			self.personaje.saltar(self.personaje.salto)
			self.personaje.reproducirAccion(SALTO2)
			if (bge.logic.globalDict["JuegoLucha"]["juego"].musica == True):
				self.personaje.sonidoSalto.reproducir(1)
			else:
				self.personaje.sonidoSalto.reproducir(0)
			self.saltando = True
			#self.personaje.cubo.collisionCallbacks.append(aterriza) #con esto ahorraria update
		#print("aire")

	def golpe1(self):
		datos = [self.saltando,self.enAire,self.cayendo]
		self.establecerEstado(LuchadorSaltandoGolpeando(self.personaje, datos))

	def update(self):
		#Comprobamos cuando el personaje deja de saltar	

		if (not self.saltando and not self.personaje.armature.isPlayingAction()):
			self.personaje.saltar(self.personaje.salto)
			self.personaje.reproducirAccion(SALTO2)
			if (bge.logic.globalDict["JuegoLucha"]["juego"].musica == True):
				self.personaje.sonidoSalto.reproducir(1)
			else:
				self.personaje.sonidoSalto.reproducir(0)
			self.saltando = True
		
		if(self.saltando and self.personaje.objeto.getLinearVelocity()[2]<-0.2):
			self.enAire =True
			#self.saltando = False

		if(self.enAire and self.personaje.objeto.getLinearVelocity()[2]>=0):
			self.enAire =False
			self.cayendo = True
			self.huesosGolpeador = []
			self.personaje.reproducirAccion(SALTO3) #esta accion no se reproduciria, habria que usar isPlayingAction o getActionFrame
			
			
		if(self.cayendo and not self.personaje.armature.isPlayingAction()):
			self.establecerEstado(LuchadorParado(self.personaje))
			#print("suelo")
			
	def updateIA(self):
		saltando(self)

class LuchadorSaltandoGolpeando(LuchadorSaltando):
	def __init__(self, personaje,datos):
		LuchadorEstado.__init__(self, personaje)
		self.saltando = datos[0]
		self.enAire = datos[1]
		self.cayendo = datos[2]
		if (bge.logic.globalDict["JuegoLucha"]["juego"].musica == True):
			self.personaje.sonidoGolpeAire.reproducir(1)
		else:
			self.personaje.sonidoGolpeAire.reproducir(0)
		self.personaje.reproducirAccion(GOLPE_SALTANDO)
		self.huesosGolpeador = [self.personaje.huesosGolpeadores[2]]
	def golpe1(self):
		pass

class LuchadorSaltandoAvanzando(LuchadorSaltando):
	def __init__(self, personaje, impulso=[0,0,.5]):
		impulso[2] *= personaje.direccionAvanzar()
		super().__init__(personaje, impulso)
		
class LuchadorSaltandoRetrocediendo(LuchadorSaltando):
	def __init__(self, personaje, impulso=[0,0,.5]):
		impulso[2] *= personaje.direccionRetroceder()
		super().__init__(personaje, impulso)

class LuchadorGolpeando(LuchadorEstado):
	def __init__(self, personaje):
		LuchadorEstado.__init__(self, personaje)
		self.tipo_golpe = .5 > random.random()
		self.inicia_golpe()
		self.saltando = True

	def inicia_golpe(self):
		self.parte = 0 # si parte ==2 que pueda hacer otra accion
		if (bge.logic.globalDict["JuegoLucha"]["juego"].musica == True):
			self.personaje.sonidoAndar.reproducir(1)
		else:
			self.personaje.sonidoAndar.reproducir(0)
		if (self.tipo_golpe):
			self.personaje.reproducirAccion(INICIO_GOLPE1)
			self.huesosGolpeador = []
		else: 
			self.personaje.reproducirAccion(INICIO_GOLPE2)
			self.huesosGolpeador = []
	
	def golpe1(self):
		if(self.parte == 2 or (self.parte == 1 and self.personaje.armature.getActionFrame()>17)):
			self.tipo_golpe = not self.tipo_golpe
			self.inicia_golpe()
		else:
			pass

	def update(self):
		#listaAcciones[Golpe1].datos["fin_golpe"]
		if (self.parte==0 and not self.personaje.armature.isPlayingAction()):
			self.parte+=1
			if (self.tipo_golpe):
				self.personaje.reproducirAccion(GOLPE1)
				self.huesosGolpeador = [self.personaje.huesosGolpeadores[0]]
			else: 
				self.personaje.reproducirAccion(GOLPE2)
				self.huesosGolpeador = [self.personaje.huesosGolpeadores[1]]

		if(round(self.personaje.armature.getActionFrame())==self.personaje.finGolpe): #getActionFrame devuelve float, y bucle se ejecuta por frame
			print("sonido golpe aire")
			if (bge.logic.globalDict["JuegoLucha"]["juego"].musica == True):
				self.personaje.sonidoGolpeAire.reproducir()
			else:
				self.personaje.sonidoGolpeAire.reproducir(0)

		if (self.parte==1 and not self.personaje.armature.isPlayingAction()):
			self.parte+=1
			if (self.tipo_golpe):
				self.personaje.reproducirAccion(FIN_GOLPE1)
				self.huesosGolpeador = []
			else: 
				self.personaje.reproducirAccion(FIN_GOLPE2)
				self.huesosGolpeador = []

		if(self.parte==2 and self.personaje.armature.isPlayingAction() == False):
			self.establecerEstado(LuchadorParado(self.personaje))
	"""
	def updateIA(self):
		Golpeando(self)
	"""		

class LuchadorGolpeado (LuchadorEstado):
	def __init__(self, personaje, protegido=False):
		LuchadorEstado.__init__(self, personaje)
		
		"""
		if (protegido):
			
			self.personaje.reproducirAccion(GOLPEADO_PROTEGIDO)
		else:
		"""
		self.personaje.reproducirAccion(GOLPEADO)
		self.personaje.perderVida(15)
		if (bge.logic.globalDict["JuegoLucha"]["juego"].musica == True):
			self.personaje.sonidoGolpear.reproducir(1)
			self.personaje.sonidoHerido.reproducir(1)
		else:
			self.personaje.sonidoGolpear.reproducir(0)
			self.personaje.sonidoHerido.reproducir(0)
		#CAMBIAR ESTO DE ABAJO POR  applyForce(force, local=False)
		self.personaje.mover([self.personaje.direccionRetroceder()*self.personaje.velocidadCorrer*5,0,0])

		

	def update(self):
		#este if else en update si esta en el init no funciona bien
		if(self.personaje.vida<=0):
			#self.ko()
			print("vida menos 0")
			self.establecerEstado(LuchadorKO(self.personaje))

		if(self.personaje.vida>0 and self.personaje.armature.isPlayingAction() == False):
			self.parar()

	#Solo puede quedar ko si ha sido golpeado
	def ko(self):
		self.establecerEstado(LuchadorKO(self.personaje))

	def parar(self):
		self.establecerEstado(LuchadorParado(self.personaje))	

	def golpeado(self):
		pass

class LuchadorProtegiendose(LuchadorEstado):
	def __init__(self, personaje):
		super().__init__(personaje)
		if (bge.logic.globalDict["JuegoLucha"]["juego"].musica == True):
			self.personaje.sonidoAndar.reproducir(1)
		self.personaje.reproducirAccion(PROTEGERSE)
		self.tiempoProtegido = 0
		

	def update(self):
		if(self.controlEstado and not self.personaje.armature.isPlayingAction()):
			self.controlEstado = False

		if(self.controlEstado and round(self.personaje.armature.getActionFrame())==3):
			if (bge.logic.globalDict["JuegoLucha"]["juego"].musica == True):
				self.personaje.sonidoBloqueo.reproducir(1)

	def updateIA(self):
		protegerse(self)
		#print("protegido: ", self.tiempoProtegido)

	def parar(self):
		if(not self.personaje.armature.isPlayingAction()):
			self.establecerEstado(LuchadorParado(self.personaje))	

	def golpe1(self):
		if(not self.personaje.armature.isPlayingAction()):
			self.establecerEstado(LuchadorGolpeando(self.personaje))

	def avanzar(self):
		if(not self.personaje.armature.isPlayingAction()):
			self.establecerEstado(LuchadorAvanzando(self.personaje))

	def retroceder(self):
		if(not self.personaje.armature.isPlayingAction()):
			self.establecerEstado(LuchadorRetrocediendo(self.personaje))

	def saltar(self):
		if(not self.personaje.armature.isPlayingAction()):
			self.establecerEstado(LuchadorSaltando(self.personaje))
	
	def golpeado(self):
		if (not self.controlEstado):
			self.controlEstado = True
			if (bge.logic.globalDict["JuegoLucha"]["juego"].musica == True):
				self.personaje.sonidoBloqueo.reproducir(1)
			self.personaje.reproducirAccion(GOLPEADO_PROTEGIDO)
	



class LuchadorKO(LuchadorEstado):
	def __init__(self, personaje):
		super().__init__(personaje)
		if (bge.logic.globalDict["JuegoLucha"]["juego"].musica == True):
			self.personaje.sonidoKo.reproducir(1)
		self.personaje.reproducirAccion(KO)
		print("ko")

	def update(self):
		("update ko")
		if(self.personaje.armature.isPlayingAction() == False):
			print("menu ko") # cuando acabe animación pantallazo de KO
			#bge.logic.globalDict["ko"] = True

	
"""
Luchadorgolpe1Saltando
"""
