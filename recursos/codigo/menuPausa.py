# -*- coding: utf-8 -*-

import bge
import os
import sys

"""
Ruta
"""
os.chdir(bge.logic.expandPath('//')) # Cambiamos la ruta del os a donde tenemos nuestro .blend
if 'codigo' not in sys.path:
	sys.path.append('codigo') #para no tener que poner from codigo.menuPrincipal import *


from menu import *

from functools import * #partial



class MenuPausa(MenuInGame):
	def __init__(self, system, data, listaElementos = [_("Reanudar"), _("Abandonar Pelea"), _("Reiniciar"),_("Salir del Juego")]): #pasar posicion centro, 
		
		super().__init__(system, data, listaElementos, centro=[.5,.4])
		print("MenuPausa creado")
		#self.juego = juego #deberia de asignarse en super()
		#Letras Pausa
		self.textoPausa = bgui.Label(self.raiz, text='Pausa', pos=[.5,.7],pt_size=120, options=bgui.BGUI_DEFAULT | bgui.BGUI_CENTERX)

		print("listaElementos[0]: ", listaElementos[0])
		self.botones.listaElementos[listaElementos[0]].on_release = self.pausa
		self.botones.listaElementos[listaElementos[1]].on_release = self.menuPrincipal
		self.botones.listaElementos[listaElementos[2]].on_release = self.reiniciarJuego
		self.botones.listaElementos[listaElementos[3]].on_release = self.salirJuego

		self.ocultar()
		

		
