# -*- coding: utf-8 -*-


#Esto habra que cambiarlo por menu
from menu import *
import menuPrincipal
from debug import *
#from juego import * #cuidado

import sys

import gettext
from functools import *#partial
import bge

PLAYER = True
CPU = False


"""
Hago menú en "2 capas"? Primera las imagenes-botones con los personajes y la segunda con los botones atras - adelante y una clase que cree a ambas?
"""

# consultar parámetro en imagenes o widget 
# aspect – constrain the widget size to a specified aspect ratio
#@ticRate(1)
class MenuEleccion(Menu):
	@postfuncion
	def __init__(self, system, data, espacioFotos=.05, listaPersonajes=["SpaceWarrior", "RockGolem"], listaEscenarios=["beach", "wasteland", "manicomio"]):
		super().__init__(system)
		self.idioma = data["idioma"]
		self.juego = data["juego"]
		#directorio = bge.logic.expandPath("//")+"traducciones" #Revisar esto
		#print("directorio traducciones: ", directorio)
		"""
		lang = gettext.translation('menuPrincipal', localedir=directorio, languages=[data["idioma"]])
		lang.install()
		_ = lang.gettext
		print("creado")
		"""
		for blend in bge.logic.LibList():
			bge.logic.LibFree(blend)
		self.listaEscenarios = listaEscenarios
		
		self.escenario = None#basta con asignarlo cuando se pulse el botón listo
		self.personaje1 = None
		self.personaje2 = None
		
		fondo = bge.logic.expandPath("//")+"imagenes/tekstuur_2.png"
		fondoEscenario = bge.logic.expandPath("//")+"imagenes/map_10.png"
		fondoPersonajes = bge.logic.expandPath("//")+"imagenes/cuadro_mosaico_personajes.png"
		fondoPersElegido = bge.logic.expandPath("//")+"imagenes/recuadro_personaje.png"
		
		
		self.fondoMenu = bgui.image.Image(self.raiz ,fondo, size=[1, 1], pos=[0, 0])

		#bge.logic.setLogicTicRate(5)
		
		self.escenarios = ElementoMenu(self.raiz, "escenarios", aspecto=None, borde=0,size=[.99, .45], pos=[.005, .5]) # pos=[0,0] esquina inferior izquierda?
		#self.escenarios.listaElementos["img"] = bgui.image.Image(self.escenarios.frame, fondoEscenario, aspect=None,size=[.75, 1], pos=[.125, 0.03])

		
		self.playerLeft = bgui.frame_button.FrameButton(self.raiz, name="playerLeft", base_color=[0,0,0,.5], text=_("Player"), font=None, pt_size=None, aspect=None, size=[.12, .05], pos=[.03, .475], sub_theme='', options=0)
		self.cpuLeft = bgui.frame_button.FrameButton(self.raiz, name="cpuLeft", base_color=[0,0,0,.25], text=_("CPU"), font=None, pt_size=None, aspect=None, size=[.12, .05], pos=[.03+.13, .475], sub_theme='', options=0)
		self.descripcionIzq = ElementoMenu(self.raiz, "descripcionIzq", borde=1, size=[.25, .275], pos=[.03,.175]) #, aspecto=4/3
		self.descripcionIzq.listaElementos["img"] = bgui.image.Image(self.descripcionIzq.frame, fondoPersElegido, aspect=None,size=[1, 1], pos=[0, 0])
		
		self.playerRight = bgui.frame_button.FrameButton(self.raiz, name="playerRight", base_color=[0,0,0,.25], text=_("Player"), font=None, pt_size=None, aspect=None, size=[.12, .05], pos=[.73, .475], sub_theme='', options=0)
		self.cpuRight = bgui.frame_button.FrameButton(self.raiz, name="cpuRight", base_color=[0,0,0,.5], text=_("CPU"), font=None, pt_size=None, aspect=None, size=[.12, .05], pos=[.73+.13, .475], sub_theme='', options=0)
		self.descripcionDer = ElementoMenu(self.raiz, "descripcionDer", borde=1, size=[.25, .275], pos=[.73,.175]) #, aspecto =4/3
		self.descripcionDer.listaElementos["img"] = bgui.image.Image(self.descripcionDer.frame, fondoPersElegido, aspect=None,size=[1, 1], pos=[0, 0])

		self.botones = ElementoMenu(self.raiz, "botones", borde=1)
		
		#############
		#	Escenarios		#
		#############
		#self.textoEscenario = bgui.text_block.TextBlock(self.escenarios.frame, text='Nombre Escenario',size=[.35, .1], pos=[.32, .9], options=bgui.BGUI_DEFAULT | bgui.BGUI_CENTERX)
		self.rutaFotoEscenarios = "imagenes/fotoEscenario/"
		#Los escenarios me gustaria que fueran un frame con recuadro, al menos el central
		self.escenarios.listaElementos["central"] = bgui.image.Image(self.escenarios.frame, self.rutaFotoEscenarios+listaEscenarios[1]+".png", name=listaEscenarios[1], size=[.35,.8], pos=[.325,.1]) #aspect = 0 => 16:9 Usar esto solo con widgets tamaño  [1, 1]
		#self.escenarios.listaElementos["central"].z_index = 1
		#recuadro 4_point_frame.png
		recuadroEscenario = bge.logic.expandPath("//")+"imagenes/point_frame.png"
		self.escenarios.listaElementos["recuadroIzq"] = bgui.image.Image(self.escenarios.frame,recuadroEscenario, size=[.24, .432], pos=[.015,.25]) #, aspect=16/9
		self.escenarios.listaElementos["izq"] = bgui.image_button.ImageButton(self.escenarios.listaElementos["recuadroIzq"], name=listaEscenarios[0], default_image=[self.rutaFotoEscenarios+listaEscenarios[0]+".png", 0,0,1,1], size=[.825, .825], pos=[.0875,.0875]) #, aspect=16/9
		#self.escenarios.listaElementos["izq"].z_index = 1
		self.escenarios.listaElementos["recuadroDer"] = bgui.image.Image(self.escenarios.frame,recuadroEscenario, size=[.24, .432], pos=[.755, .25])#, aspect=16/9
		self.escenarios.listaElementos["der"] = bgui.image_button.ImageButton(self.escenarios.listaElementos["recuadroDer"], name=listaEscenarios[2], default_image=[self.rutaFotoEscenarios+listaEscenarios[2]+".png", 0,0,1,1], size=[.825, .825], pos=[.0875,.0875]) #, aspect=16/9
		#self.escenarios.listaElementos["der"].z_index = 1
		
		self.escenarios.listaElementos["izq"]._on_click = self.rotarEscenariosIzq
		self.escenarios.listaElementos["der"]._on_release = self.rotarEscenariosDer #Porque con frameButton no hay que poner el primer _ de _on_release?
		self.raiz.z_index = 0
		self.escenarios.frame.z_index=1

		
		#self.escenarios.listaElementos["img"].visible = False
		#self.escenarios.listaElementos["img"].z_index = 0

		#############
		#	Personajes		#
		#############

		self.recuadroPersonajes = ElementoMenu(self.raiz, "recuadroPersonajes", borde=3, size=[.4, .4], pos=[.3, .075])
		#self.recuadroPersonajes.listaElementos["fondo"] = bgui.image.Image(self.recuadroPersonajes.frame, fondoPersonajes, aspect=None,size=[1, 1], pos=[0, 0])
		posActual = 0
		self.rutaFotoPersonaje = "imagenes/fotoPersonaje/"
		personajeDesconocido = bge.logic.expandPath("//")+"imagenes/fotoPersonaje/mono-unknown.png"
		
		for personaje in listaPersonajes:
			x = .015 + (posActual%4)*.2 + (posActual%4)*0.05
			y = .675 - (posActual//4)*.35 - (posActual//4)*0.025
			
			print(personaje, x, y, posActual)
			
			self.recuadroPersonajes.listaElementos[personaje] = bgui.image_button.ImageButton(self.recuadroPersonajes.frame, name=personaje, default_image=[self.rutaFotoPersonaje+personaje+".png", 0,0,1,1], size=[.3*3/4,.3], pos=[x,y]) #aspect=4/3
			self.recuadroPersonajes.listaElementos[personaje].on_click = partial(self.seleccionaPersonaje, personaje)
			self.recuadroPersonajes.listaElementos[personaje].on_mouse_enter = partial(self.tamPersonaje, personaje, [.31*3/4,.31]) #Esta medida se debe al aspecto
			self.recuadroPersonajes.listaElementos[personaje].on_mouse_exit = partial(self.tamPersonaje, personaje, [.3*3/4,.3])
			#self.recuadroPersonajes.listaElementos[personaje].on_hover # agrandar imagen y sonido// a lo mejor es  on_mouse_enter  on_mouse_exit

			posActual = posActual + 1
		"""
		#Huecos de personajes vacio
		for personaje in range(0,10):
			x = 0.015+ (posActual%4)*.2 + (posActual%4)*0.05
			y = .675- (posActual//4)*.3 - (posActual//4)*0.025
			
			#print(personaje, x, y, posActual)
			
			self.recuadroPersonajes.listaElementos[personaje] = bgui.image_button.ImageButton(self.recuadroPersonajes.frame, name=personaje, default_image=[personajeDesconocido, 0,0,1,1], size=[.3*3/4,.3], pos=[x,y])
			#self.recuadroPersonajes.listaElementos[personaje].on_click = partial(self.seleccionaPersonaje, personaje)
			
			posActual = posActual + 1
		"""
		#############
		#	Botones		#
		#############
		#Botón atrás o volver, para regresar al menu principal
		self.botones.anadirBoton(_("Volver"), pos=[.025, .05], size=[.25,.1])
		

		#Botón siguiente, que en principio este desactivado y se active cuando estén seleccionado los 2 personajes. Mandará a menu seleccionar escenario
		self.botones.anadirBoton(_("Listo"),pos=[.725,.05], size=[.25,.1])
	
		#self.botones.listaElementos["Listo"].visible = False
		self.botones.listaElementos[_("Listo")].frozen = True #probar con uno solo
		
		
		self.botones.listaElementos[_("Listo")].on_release = self.empezarJuego
		self.botones.listaElementos[_("Volver")].on_release = self.volver

		self.cpuLeft.on_release = self.cpu_player
		self.playerLeft.on_release = self.cpu_player
		self.cpuRight.on_release = self.cpu_player
		self.playerRight.on_release = self.cpu_player

		self.controls = [PLAYER, CPU]

	def cpu_player(self, widget):

		if (widget.name == 'playerLeft'):
			self.controls[0] = PLAYER
			#self.player=[True,False]
			self.playerLeft.color = [0,0,0,.5]
			self.cpuLeft.color = [0,0,0,.25]
		elif (widget.name == 'cpuLeft'):
			self.controls[0] = CPU
			#self.player=[True,False]
			self.playerLeft.color = [0,0,0,.25]
			self.cpuLeft.color = [0,0,0,.5]
		elif (widget.name == 'playerRight'):
			self.controls[1] = PLAYER
			#self.player=[True,False]
			self.playerRight.color = [0,0,0,.5]
			self.cpuRight.color = [0,0,0,.25]
		elif (widget.name == 'cpuRight'):
			self.controls[1] = CPU
			#self.player=[True,False]
			self.playerRight.color = [0,0,0,.25]
			self.cpuRight.color = [0,0,0,.5]
		
	def volver(self, widget):
		print("System: ", self.system)
		self.system.load_layout(menuPrincipal.MenuPrincipal, {"idioma": self.idioma, "juego": self.juego})
		#self.juego.load_layout(MenuPrincipal, {"juego": self.juego, "idioma":self.idioma})
		#self.system.cambiarPantalla(MenuPrincipal, {"juego": self.system, "idioma":self.idioma})
		"""
		File "/home/p9/Escritorio/pfc2/recursos/librerias/bgui_master/bgui/widget.py", line 77, in __call__
		    self.f(*((self.c(),) + args))
		  File "codigo/menuEleccion.py", line 102, in volver
		    self.system.load_layout(MenuPrincipal(), self.juego)
		NameError: global name 'MenuPrincipal' is not defined
		"""

	#Borra el primer elemento y lo inserta al final
	def rotarEscenariosDer(self, widget): #CUIDADO con poner widget!!
		#valor auxiliar para guardar la ultima
		print("longitud lista elementos: ", len(self.listaEscenarios))
		self.listaEscenarios.append(self.listaEscenarios[0])
		self.listaEscenarios.pop(0)
		self.actualizarEscenarios()
	
	#Borra el último elemento y lo inserta al principio
	def rotarEscenariosIzq(self, widget):
		print("rota")
		self.listaEscenarios.insert(0,self.listaEscenarios[len(self.listaEscenarios)-1])
		self.listaEscenarios.pop(len(self.listaEscenarios)-1)		
		self.actualizarEscenarios()
		
	def actualizarEscenarios(self):
		self.escenarios.listaElementos["izq"].update_image(self.rutaFotoEscenarios+self.listaEscenarios[0]+".png")
		self.escenarios.listaElementos["central"].update_image(self.rutaFotoEscenarios+self.listaEscenarios[1]+".png")
		self.escenarios.listaElementos["der"].update_image(self.rutaFotoEscenarios+self.listaEscenarios[2]+".png")
		
	def seleccionaPersonaje(self, nombre, widget): #Widget tiene que ser último parámetro
		if (self.personaje1 == None):
			self.personaje1 = nombre
			print(self.personaje1)
			self.descripcionIzq.listaElementos["desIzq"] = bgui.image.Image(self.descripcionIzq.frame, self.rutaFotoPersonaje+self.personaje1+".png", name=self.personaje1,size=[0.95,.95], pos=[.025,.025]) #aspect = 0 => 16:9 Usar esto solo con widgets tamaño  [1, 1]
		elif (self.personaje2 == None):
			self.personaje2 = nombre
			print(self.personaje2)
			self.descripcionDer.listaElementos["desDer"] = bgui.image.Image(self.descripcionDer.frame, self.rutaFotoPersonaje+self.personaje2+".png", name=self.personaje1,size=[0.95,.95], pos=[.025,.025]) #aspect = 0 => 16:9 Usar esto solo con widgets tamaño  [1, 1]
			self.botones.listaElementos[_("Listo")].frozen = False
		else:
			#self.botones.listaElementos["Listo"].visible = True
			
			print("ambos personajes seleccionados")
			
	def empezarJuego(self, widget):
		self.escenario = self.listaEscenarios[1]
		#self.personaje2 = "demonio" #CAMBIAR ESTO
		#self.crearJuego(self.personaje1, self.personaje2, self.escenario)
		bge.logic.globalDict["JuegoLucha"] = {"juego":self.juego,"personaje1":self.personaje1,"personaje2":self.personaje2,"escenario":self.escenario, "controlados":self.controls}
		print("MenuEleccion.empezarjueg():ruta sys.path: ", sys.path)
		self.system.cambiarPantalla(PantallaCargando)
		#self.juego.escena = PantallaCargando(self.juego,self.personaje1,self.personaje2,self.escenario)
			
	#Cuidado con el aspecto!
	def tamPersonaje(self, x,tam, widget):
		self.recuadroPersonajes.listaElementos[x].size=tam

	def update(self):
		#print("MenuEleccion update")
		pass
			
"""
Seguramente ponga este menú encima del de eleccion de personaje por tanto tenerlo en cuenta a la hora de diseñarlo.

El diseño es en el centro la imagen del escenario seleccionado a izquierda y derecha mas pequeño (y posiblemente mas claro) otros dos escenarios y unas felchas para izquierda y derecha de manera que vayan rotando. Abajo los botones atrás y listo. 
"""

