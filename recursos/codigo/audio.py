# -*- coding: utf-8 -*-

import bge

import aud

#¿Crear un dispositivo por sonido?
class Sonido():
	def __init__(self, archivo):
		self.device = aud.device() #probar si funciona
		directorio = bge.logic.expandPath("//")
		self.archivo = aud.Factory(directorio+"sonidos/" + archivo)

	def reproducir(self, volumen=.5):
		self.sonido = self.device.play(self.archivo)
		self.sonido.volume = volumen

	def playLoop(self, volumen=.5):
		self.archivo = self.archivo.loop(-1)
		self.sonido = self.device.play(self.archivo)
		#self.sonido.keep = True
		self.sonido.volume = volumen
		
	def stop(self):
		self.sonido.stop()
		


"""
class Audio():
	def __init__(self):
		#dispositivo de audio		
		self.device = aud.device()
		self.audios = {} #self.factory
		self.controlAudio = {} #
		
	def cargaAudio(self, archivo): #archivo o ruta
		self.audios["archivo"] = aud.Factory("sonidos/" + archivo)
		return self.audios["archivo"]
		
	#cambiarlo para que acepte sonido.reproducir()
	def reproducirAudio(self, archivo, volumen=.5):
		# play the audio, this return a handle to control play/pause
		if (archivo not in self.audios):
			print("ERROR reproducirAudio(): archivo no cargado")
		else:
			self.controlAudio[archivo] = self.device.play(self.audios["archivo"])
			self.controlAudio[archivo].volume = volumen #maximo 1.0
"""